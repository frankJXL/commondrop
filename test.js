$(function() {

	var countries = [
		"Portugal long long long long ",
		"Rico loooooooooooooong",
		"Qatar",
		"Romania",
		"Russia",
		"Rwanda",
		"Nevis",
		"Lucia",
		"Grenadines",
		"Samoa",
		"Marino",
		"Principe",
		"Arabia",
		"Senegal",
		"Montenegro",
		"Seychelles",
		"Leone",
		"Singapore",
		"Slovakia",
		"Slovenia",
		"Islands",
		"Somalia",
		"Africa",
		"Spain",
		"Lanka",
		"Sudan",
		"South",
		"Suriname",
		"Swaziland",
		"Sweden",
		"Switzerland",
		"Syria",
		"Taiwan",
		"Tajikistan",
		"Tanzania",
		"Thailand",
		"Togo",
		"Tonga",
		"Tobago",
		"Tunisia",
		"Turkey"
	];

	var option = [
		"Option 0",
		"Option 1",
		"Option 2",
		"Option 3",
		"Option 4",
		"Option 5",
		"Option 6",
		"Option 7",
		"Option 8",
		"Option 9",
		"Option 10",
		"Option 11",
		"Option 12",
		"Option 13",
		"Option 14",
		"Option 15",
		"Option 16",
		"Option 17",
		"Option 18",
		"Option 19",
		"Option 20",
		"Option 21",
		"Option 22",
		"Option 23",
		"Option 24",
		"Option 25",
		"Option 26",
		"Option 27",
		"Option 28",
		"Option 29",
		"Option 30",
		"Option 31",
		"Option 32",
		"Option 33",
		"Option 34",
		"Option 35",
		"Option 36",
		"Option 37",
		"Option 38",
		"Option 39",
		"Option 40"
	];

	// Commondrop Single
	for(var i = 0; i < 1; i++) {
		var $container = $("<div></div>").addClass("demo-container");
		var $select = $("<select multiple></select>");
		var $option = $("<option></option>");
			$option.val("");
			$option.text("");
			$select.append($option);
		$.each(countries, function(i, country) {
			var $option = $("<option></option>");
			$option.val(i);
			$option.text(country);
			$select.append($option);
		});
		$container.append($select);
		$("body").append($container);
		$select.commondrop({
			enable_status: false,
			enable_modifiers: false,
			single_placeholder_text: "Select an option",
			placeholder_option_text: "-",
			width: 100
		}).addClass("A");
	}

	// Commondrop Fetch
	for(var k = 0; k < 1; k++) {
		var $container = $("<div></div>").addClass("demo-container");
		var $select = $("<select></select>");
		
		$container.append($select);
		$("body").append($container);
		$select.commondropfetch({
			enable_search: false,
			enable_modifiers: false,
			enable_status: false,
			display_mode: 'tags',
			width: 500,
			on_reposition: function($dropdown, $display) {
				/*if($dropdown) {
					$("body").append($dropdown);
					$dropdown.css("left", 30);
	                $dropdown.css("top", 30);
	            }*/
			},
			fetch_procedure_value: function(params) {
				var dfd = new $.Deferred();
				var ref = [];

				setTimeout(function() {
					for (var i = 0; i < params.values.length; i++) {
						ref.push({
							text: "Value " + i,
							value: i
						});
					}
					dfd.resolve(ref);
				}, 500);

				return dfd.promise();

			},
			fetch_procedure_cluster: function(params) {
				var dfd = new $.Deferred();
				var ref = [];
				var total = 214;
				var nextOffset = 0;

				console.log("fetch params:", params);

				if(params.offset + params.limit >= total) {
					nextOffset = total;
				} else {
					nextOffset = params.offset + params.limit;
				}

				setTimeout(function() {
					for (var i = params.offset; i < nextOffset; i++) {
						ref.push({
							text: "Option " + i,
							value: i
						});
					}
					var data = {
						data: ref,
						offset: params.offset + params.limit,
						total: total
					};
					dfd.resolve(data);
				}, 500);
				return dfd.promise();
			},

			fetch_procedure_search: function(params) {
				var dfd = new $.Deferred();
				var ref = [];
				var total = 214;
				var nextOffset = 0;

				console.log("fetch params:", params);
				
				if(params.offset + params.limit > total) {
					nextOffset = total;
				} else {
					nextOffset = params.offset + params.limit;
				}

				setTimeout(function() {
					for (var i = params.offset; i < nextOffset; i++) {
						ref.push({
							text: "Search " + i,
							value: "search" + i
						});
					}
					var data = {
						data: ref,
						offset: params.offset + params.limit,
						total: total
					};
					dfd.resolve(data);
				}, 500);
				return dfd.promise();
			}
		}).addClass("B");
	}

	// Commondrop Multiple
	for(var j = 0; j < 1; j++) {
		var $container = $("<div></div>").addClass("demo-container");
		var $select = $("<select multiple></select>");	
		for(var repeat = 0; repeat < 1; repeat++) {
			$.each(option, function(i, option) {
				var $option = $("<option></option>");
				$option.val(i);
				$option.text(option);
				if(i % 3 % 2) $option.prop("selected", true);
				$select.append($option);
			});
		}
		$container.append($select);
		$("body").append($container);
		$select.commondrop({
			mode_override: true,
			selection_mode: 'multiple',
			enable_search: true,
			enable_status: true,
			pin_selected: true,
			enable_bulk_select: true
		}).addClass("C");
	}


	$(".D").commondrop({
		enable_groups: true,
		width: 100
	});

	$(".A").on("change", function(ev, params) {
		console.log("CHANGED", params);
		console.log($(this).commondrop("value"));
		console.log($(this).val());
	});

	$(".B").on("change", function(ev, params) {
		console.log("CHANGED", params);
		console.log($(this).commondropfetch("value"));
	});

	$(".C").on("change", function(ev, params) {
		console.log("CHANGED", params);
		console.log($(this).commondrop("value"));
	});

	$(".D").on("change", function(ev, params) {
		console.log("CHANGED", params);
		console.log($(this).commondrop("value"));
	});
});