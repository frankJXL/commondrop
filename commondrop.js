/* ===============================================================================
  _____                           ___              
 / ___/__  __ _  __ _  ___  ___  / _ \_______  ___ 
/ /__/ _ \/  ' \/  ' \/ _ \/ _ \/ // / __/ _ \/ _ \
\___/\___/_/_/_/_/_/_/\___/_//_/____/_/  \___/ .__/
                                            /_/    
                                            
Commondrop - Multipurpose Dropdown
by Frank Liu for Veeva Network

Version 0.1

See usage and documentation at 

!! DO NOT DIRECTLY EDIT THIS FILE !!

================================================================================ */

(function($, window, document, undefined) {
	$.widget("custom.commondrop", {
		//Options to be used as defaults
		options: {
			// modes
			display_mode: 'count', // tags, count 
			selection_mode: 'single', // single, multiple
			search_timer: null,
			search_delay: 300,											// time it takes to 
			output_logs: true,											// flag to enable console log outputs

			// elements
			$select: null,
			$display: null,
			$dropdown: null,
			$empty: null,
			$emptyResult: null,
			$search: null,

			// styles
			width: null,

			// feature flags
			enable_groups: false,										// enable optgroup support
			data_override: false,										// ignore options in select if data is provided on init
			keep_options: false,										// keep options in original select and reflect data changes on them
			enable_search: true,										// enable search box in the dropdown
			enable_action_button: false,								// enable the single action button in the dropdown
			enable_modifiers: true,										// enable group modifiers in the dropdown
			enable_option_modifier: true,								// enable single option modifiers in the dropdown
			enable_group_modifier: true,								// enable group modifiers in the dropdown
			enable_status: true,										// enable status indicator on the dropdown
			enable_bulk_select: false,									// enable paste bulk select functionality
			enable_icons: false,										// enable icon prefix for options
			pin_selected: false,										// pin selected items to the top of the list
			inherit_width: false,										// inherit <select> width to size the dropdown
			single_placeholder_text: "-",								// placeholder text for single dropdown
			multiple_placeholder_text: "No options selected",			// placeholder text for multiple dropdown
			placeholder_option_text: "No value",						// placeholder text for the empty option
			search_placeholder_text: "Search",							// placeholder text for search input
			empty_options_text: "No options available",					// placeholder for empty-box when no options exist
			empty_results_text: "No matching results",					// placeholder for empty-result-box when no options exist
			destroy_on_close: false,									// flag to indicate if the dropdown is to be recreated upon every open, and destroyed on every close
			trigger_change: true,										// trigger change on the original select element when commonDrop options change
			
			// callbacks
			on_open: null,
			on_close: null,
			on_reposition: null,

			// data
			data: [],

			// states
			state: 'loaded',										    // loaded, loading, search, search-loading
			initially_loaded: false,									// set to true when first loaded
			isLoading: false,											// semaphore to control loading proceduress
			isSearch: false,
			isDisabled: false,											// if the entire dropdown is disabled
			index: 0, 													// the index of the highlighted option
			color_status: null,											// display a colored border for warning / error / success state
			// component templates
			display_template: null,										// dom template for replacing commondrop rendered display
			dropdown_template: null,									// dom tmeplate for replacing common rendered dropdown
			option_template: null,										// dom template for replacing option template
			
			data_template: {											// a single object representation of an option
				id: null, 												// UUID given to identify the option
				groupId: null,											// the group ID that this option belongs in, null indicates it's not part of a group
				text: "Untitled Option", 								// The displayed text
				value: null, 											// The value of the option
				selected: false, 										// Flag indicating if the option is selected
				disabled: false, 										// Flag indicating if the option is diabled
				hidden: false, 											// Flag indicating if the option is hidden (but exists in DOM)
				render: true,											// Flag indicating if the option is rendered in DOM
				sort: 0,												// Sort order of the options to exist in the dropdown
				icon: [],												// An array of icon classes for the span
				metadata: null	 										// JSON containing any metadata for filter purposes
			},
			group_template: {
				id: null,
				text: "Untitled Group",
				value: null,
				disabled: false,
				hidden: false,
				render: true,
				sort: 0,
				icon: [],
				data: [],
				metadata: null
			},
			// feature data
			bulk_select_trigger_word: ":list:",
			bulk_outputs: null,

			// customizable elements
			action_button: {
				text: 'Done',
				action: function(self) {
					self.close();
				}
			},
			modifiers: [{
				text: "Select All",
				action: function(data, $modifier, isGrouped) {
					// select all, change to select none
					if ($modifier.hasClass('all') || !$modifier.hasClass('none')) {
						$modifier.removeClass('all').addClass('none');
						$modifier.text("Select None");
						if(isGrouped) {
							$.each(data, function(i, group) {
								$.each(group.data, function(j, option) {
									if(option.render) option.selected = true;
								});
							});
						} else {
							$.each(data, function(i, option) {
								if(option.render) option.selected = true;
							});
						}

						// select none, change to select all
					} else {
						$modifier.addClass('all').removeClass('none');
						$modifier.text("Select All");
						if(isGrouped) {
							$.each(data, function(i, group) {
								$.each(group.data, function(j, option) {
									if(option.render) option.selected = false;
								});
							});
						} else {
							$.each(data, function(i, option) {
								if(option.render) option.selected = false;
							});
						}
					}
				}
			}],
			option_modifier: {
				text: 'Only',
				action: function(data, $option, isGrouped) {
					var thisId = $option.attr("option-id");
					if(isGrouped) {
						$.each(data, function(i, group) {
							$.each(group.data, function(j, option) {
								if (option.id == thisId) {
									option.selected = true;
								} else {
									option.selected = false;
								}
							});
						});
					} else {
						$.each(data, function(i, option) {
							if (option.id == thisId) {
								option.selected = true;
							} else {
								option.selected = false;
							}
						});
					}
				}
			},
			group_modifier: {
				text: 'Select All',
				action: function(data, $group) {
					var thisId = $group.attr("group-id");
					var $modifier = $group.find("a");
					if($modifier.hasClass("all") || !$modifier.hasClass('none')) {
						$modifier.removeClass("all").addClass('none');
						$modifier.text("Select None");

						$.each(data, function(i, group) {
							if (group.id == thisId) {
								$.each(group.data, function(j, option) {
									option.selected = true;
								});
								return false; // exit
							}
						});
					} else {
						$modifier.removeClass("none").addClass('all');
						$modifier.text("Select All");
						$.each(data, function(i, group) {
							if (group.id == thisId) {
								$.each(group.data, function(j, option) {
									option.selected = false;
								});
								return false; // exit
							}
						});
					}
				}
			}
		},

		/**
		 * Enable/Disable features based on existing configuration
		 *
		 * Some features cannot be turned on while others are enabled
		 * this function will disable or enable features that collide with
		 * each other
		 */
		featureCrossCheck: function() {
			var self = this;
			// modifier box contains both status and modifier
			// enabling status requires the modifier box to be appended
			if(self.options.enable_status) {
				self.options.enable_modifiers = true;
			}

			// bulk select is unnecessary in single selection mode
			// since only one option can be selected
			if(self.options.selection_mode == 'single') {
				self.options.enable_bulk_select = false;
				self.options.enable_option_modifier = false;
			}

			if(self.options.selection_mode == 'single') {
				self.options.enable_group_modifier = false;
			}

			// when optgroups are enabled, options remain pinned to their
			// own groups
			if(self.options.enable_groups) {
				self.options.pin_selected = false;
			}

			if(self.options.selection_mode == 'multiple') {
				self.options.enable_empty_option = false;
			}
		},

		/**
		 * Main initialization procedure. Creates dropdown and display.
		 *
		 * Adds select options into the data set by creating commondrop option
		 * objects, starts off main event loop after creating options.
		 */
		_create: function() {
			var self = this;
			var groups = [];
			var data = [];
			var createOptionObj = function(option, params) {
				return $.extend(true, {}, self.options.data_template, {
					id: self.guid(),
					text: option ? option.text() : null,
					value: option ? option.val() : null,
					selected: option ? option.prop("selected") : false,
					disabled: option ? option.prop("disabled") : false
				}, params);
			};

			var createGroupObj = function(group, params) {
				return $.extend(true, {}, self.options.group_template, {
					id: self.guid(),
					text: group ? group.attr("label") : "",
					disabled: group ? group.prop("disabled"): false
				}, params);
			};
			// create a pointer to the original element
			self.options.$select = this.element;
			self.options.$select.addClass("commondrop-enabled");

			// set default states
			if (self.options.$select.attr("multiple")) {
				this.options.selection_mode = 'multiple';
			} else {
				this.options.selection_mode = 'single';
			}

			// prepare data set
			if(!self.options.data_override) {
				// if groups are enabled, iterate through the dropdown and collect all groups
				// and options
				if(self.options.enable_groups) {
					self.options.$select.find("> optgroup").each(function(i) {
						// create new group
						var $optgroup = $(this);
						var newGroupId = self.guid();
						var groupData = [];
						var newGroup = createGroupObj($optgroup, {
							id: newGroupId,
							sort: i
						});
					
						// create options in optgroups and then push them to the new group
						$optgroup.find("> option").each(function(i) {
							groupData.push(createOptionObj($(this), {
								groupId: newGroupId,
								sort: i
							}));
						});

						newGroup.data = groupData;
						groups.push(newGroup);
					});

					// gather all ungrouped options in a group as well
					var ungrouped = createGroupObj(null, {
						text: "Ungrouped",
						id: self.guid()
					});

					// create options that are not in optgroups
					self.options.$select.find("> option").each(function(i) {
						// skip empty options
						if (!$(this).val()) return true;
						ungrouped.data.push(createOptionObj($(this), {
							groupId: ungrouped.id,
							sort: i
						}));
					});

					groups.push(ungrouped);
					// save groups into data
					self.options.data = groups;
				} else {
					self.options.$select.find("option").each(function(i) {
						// create a special empty option if no value or text exist
						if (!$(this).val() && !$(this).text()) {
							if(self.options.selection_mode == 'single') {
								data.push(createOptionObj($(this), {
									text: self.options.placeholder_option_text,
									sort: i,
									isPlaceholder: true
								}));
							}
						} else {
							data.push(createOptionObj($(this), {
								sort: i
							}));
						}
					});
					self.options.data = data;
				}
			}

			self.options.$select.css({
				visibility: 'none' // needed to calculate location of commondrop, hide after render
			});

			// prepare modifiers
			$.each(self.options.modifiers, function(i, modifier) {
				modifier.id = self.guid();
			});

			// remove all unselected options if keep option is disabled
			if(!self.options.keep_options) self.setSelect();
			// setting initally loaded to true immediately since options are created right away
			self.options.initially_loaded = true;
			/* ------------------------------------------------------------------- */
			/* MAIN EXECUTION
			/* ------------------------------------------------------------------- */
			// create the display element
			self.featureCrossCheck();
			self.options.$display = self._createDisplay();
			self.element.after(self.options.$display);
			self.options.$select.hide();
			self.repositionSearch();
			self.updateDisplay();
			self._attachHandlers();
			self.setColorStatus();
			/* ------------------------------------------------------------------- */
		},

		/**
		 * Destroys the commondrop object
		 *
		 * Destroys commondrop and it's associated data. Events are also
		 * unbound for global objects such as window.
		 */
		destroy: function() {
			var closeFn = self.close;
			var onCloseOtherFn = self.onCloseOther;
			// detach any window events
			$(window).off("resize", closeFn);
			$(window).off("click", closeFn);
			$(window).off("commondrop:close_other", onCloseOtherFn);
			// For UI 1.8, destroy must be invoked from the
			// base widget
			$.Widget.prototype.destroy.call(this);
			// For UI 1.9, define _destroy instead and don't
			// worry about
			// calling the base widget
		},

		/**
		 * Attaches events and callbacks
		 *
		 * Attaches events and callbacks to global objects that reacts
		 * to changes in commondrop
		 */
		_attachHandlers: function() {
			var self = this;
			var $select = self.options.$select;
			$(window).on('resize', $.proxy(self.close, self));
			$(window).on('click', $.proxy(self.close, self));
			$(window).on("commondrop:close_other", function(ev, data) {
				if(data && data.parent != self) self.close();
			});

			$select.on("commondrop:open", $.proxy(self.open, self));
			$select.on("commondrop:close", $.proxy(self.close, self));
			$select.on("commondrop:updateDisplay", $.proxy(self.updateDisplay, self));
			$select.on("commondrop:updateDropdown", $.proxy(self.updateDropdown, self));

			// set disable properties
			if(self.options.$select.prop("disabled")) {
				self.disable();
			} else {
				self.enable();
			}
		},

		/**
		 * Create empty placeholder element for dropdown
		 *
		 * Creates the empty placeholder element for dropdown
		 * when no options exist
		 */
		_createEmpty: function() {
			var self = this;
			var $dropdown = self.options.$dropdown;
			var $empty = $("<div></div>").addClass("empty-box");
			
			$empty.text(self.options.empty_options_text);
			$dropdown.find(".empty-box").remove();
			$dropdown.append($empty);

			self.options.$empty = $empty;
		},

		/**
		 * Destroy empty placeholder element for dropdown
		 *
		 * Destroys the empty placeholder elements and remove it's
		 * reference from options
		 */
		_destroyEmpty: function() {
			var self = this;
			var $empty = self.options.$empty;
			if(!$empty) return;
			self.options.$empty.remove();
			self.options.$empty = null;
		},

		/**
		 * Create empty result placeholder element for dropdow
		 *
		 * Creates the empty placeholder element for dropdown
		 * when no options are returned after searching
		 */
		_createEmptyResult: function() {
			var self = this;
			var $dropdown = self.options.$dropdown;
			var $emptyResult = $("<div></div>").addClass("empty-result-box");
			
			$emptyResult.text(self.options.empty_results_text);
			$dropdown.find(".empty-result-box").remove();
			$dropdown.append($emptyResult);

			self.options.$emptyResult = $emptyResult;
		},

		/**
		 * Destroy empty result placeholder element for dropdown
		 *
		 * Destroys the empty placeholder elements and remove it's
		 * reference from options
		 */
		_destroyEmptyResult: function() {
			var self = this;
			var $emptyResult = self.options.$emptyResult;
			if(!$emptyResult) return;
			self.options.$emptyResult.remove();
			self.options.$emptyResult = null;
		},

		/**
		 * Construct display element
		 *
		 * Creates the DOM element for commondrop display, and returns
		 * the completed display element
		 *
		 * @return {element} the display element
		 */
		renderDisplay: function(){
			var self = this;
			var $display = 		$("<div></div>").addClass("commondrop-display");
			var $arrow = 		$("<div />").addClass("arrow down");
			var $label = 		$("<span />").addClass("display-label");
			var $empty = 		$("<span />").addClass("display-label");
			var $search = 		$("<input type='text'>").attr("placeholder", placeholder).addClass("search-input");
			var $coverBox =		$("<div />").addClass("cover-box");

			var placeholder = "";
			if(self.options.selection_mode == 'single') {
				placeholder = self.options.search_placeholder_text;
			} else {
				if(self.options.enable_bulk_select) {
					placeholder = "search options / paste options";
				} else {
					placeholder = self.options.search_placeholder_text;
				}
			}

			switch (self.options.selection_mode) {
				case 'single':
					$display.append($label, $arrow);
					$display.addClass("single");
					break;

				case 'multiple':
					if(self.options.display_mode == 'tags') {
						$display.append($empty);
						$display.addClass("multiple");
					}
					if(self.options.display_mode == 'count') {
						$display.append($label, $arrow);
					}
					break;
			}


			// create the search box here first. Search box acts as the focus area
			// of the dropdown to enable focusable widget. The search input will move
			// from the display to the dropdown whenever the dropdown is open, and will
			// move to the display whenever the dropdown is closed.

			self.options.$search = $search;
			$display.append($search);
			$display.append($coverBox);

			return $display;
		},


		/**
		 * Create display element
		 *
		 * Creates the DOM element for commondrop display, and binds
		 * events to the display element
		 *
		 * @return {element} the display element
		 */
		_createDisplay: function() {
			var self = this;
			var $display = null;
			var $select = self.options.$select;

			// if display template is provided, override default template
			if(self.options.display_template) {
				$display = $(self.options.display_template);
			} else {
				$display = self.renderDisplay();
			}

			switch (self.options.selection_mode) {
				case 'single':
					$display.find(".display-label").text(self.options.single_placeholder_text);
					break;

				case 'multiple':
					$display.find(".display-label").text(self.options.multiple_placeholder_text);
					break;

			}

			// attach any event handlers for the display
			$select.on('commondrop:updateDisplay', $.proxy(self.updateDisplay, self));
			$display.on('click', function(ev) {
				ev.stopPropagation();
				if(!self.options.$display.find(".cover-box").hasClass("active")) self._toggleDropdown();
			});

			self.options.$search.on('keyup', $.proxy(self.onSearchKeyup, self)); // keyup to handle searching
			self.options.$search.on('keydown', $.proxy(self.onSearchKeydown, self)); // keydown to handle index navigation
			self.options.$search.on('focus', $.proxy(self.onSearchFocus, self)); 
			self.options.$search.on('blur', $.proxy(self.onSearchBlur, self));

			return $display;
		},

		/**
		 * Construct dropdown element
		 *
		 * Creates the DOM element for commondrop dropdown, and binds
		 * events to the display element
		 *
		 * @return {element} the display element
		 */
		_renderDropdown: function() {
			var self = this;
			var $search = self.options.$search;
			// create containers
			var $dropdown = 		$("<div />").addClass("commondrop-menu");
			var $bulkBox = 			$("<div />").addClass("bulk-box");
			var $searchBox = 		$("<div />").addClass("search-box");
			var $controlBox = 		$("<div />").addClass("control-box");
			var $optionsBox = 		$("<div />").addClass("options-box");
			var $coverBox =			$("<div />").addClass("cover-box");

			var $searchIcon = $("<div></div>").addClass("cd-search-icon");
			$searchBox.append($search); // move the search element from the display to the dropdown
			$searchBox.append($searchIcon); // this is an actual append
			
			// search input captures all keyup events, therefore it must be rendered, but should be hidden
			// if search is disabled
			if (!self.options.enable_search) {
				// focus can't be achieved on hidden elements, therefore, simply crop out the element
				$searchBox.css({ opacity: 0, height: 0 });
			}

			if (self.options.enable_action_button) {
				var $apply = $("<div />").addClass("action-button").text(self.options.action_button.text).addClass("active");
				$searchBox.append($apply);
			}

			if(self.options.enable_bulk_select) {
				var $dialogBtn = $("<div />").addClass("dialog-button");
				var $bulkBtn = $("<div />").addClass("bulk-button").text("Merge");

				var $selectSpan = $("<span></span>").addClass("select");
				var $unchangeSpan = $("<span></span>").addClass("unchange");
				var $unknownSpan = $("<span></span>").addClass("unknown");

				$searchBox.append($bulkBtn);
				$bulkBox.append($dialogBtn);
				$bulkBox.append($selectSpan);
				$bulkBox.append($unchangeSpan);
				$bulkBox.append($unknownSpan);
			}

			if (self.options.enable_modifiers) {
				var $modifier = $("<div />").addClass("modifiers");
				$.each(self.options.modifiers, function(i, modifier) {
					var $a = $("<a />").addClass("modifier").text(modifier.text);
					$a.attr("data-id", modifier.id);
					$a.attr("title", modifier.text);
					$controlBox.append($modifier.append($a));
					$a.on('click', $.proxy(self.onModifierClick, this, self, $a));
				});
			}

			if (self.options.enable_status) {
				var $status = $("<div />").addClass("status");
				if(!self.options.enable_modifiers) $status.css("max-width", 'none');
				$controlBox.append($status);
			}

			// append everything together
			if (self.options.enable_bulk_select) $dropdown.append($bulkBox);
			$dropdown.append($searchBox);
			if ((self.options.enable_modifiers && self.options.modifiers.length) || self.options.enable_status) {
				$dropdown.append($controlBox);
			} 

			$dropdown.append($optionsBox);
			$dropdown.append($coverBox);
			$dropdown.css({position: "absolute"});

			return $dropdown;
		},

		/**
		 * Create dropdown element
		 *
		 * Creates the DOM element for commondrop dropdown, and binds
		 * events to the dropdown element
		 *
		 * @return {element} the dropdown element
		 */
		_createDropdown: function() {

			var self = this;
			var $dropdown = null;
			var $select = self.options.$select;

			// if dropdown template is provided, override default template
			if(self.options.dropdown_template) {
				$dropdown = $(self.options.display_template);
			} else {
				$dropdown = self._renderDropdown();
			}

			// attach dropdown events
			
			$dropdown.find(".cd-search-icon").on('click', $.proxy(self.onSearchIconClick, self));

			if (self.options.enable_action_button) {
				$dropdown.find(".action-button").on('click', $.proxy(self.options.action_button.action, this, self));
			}
			if(self.options.enable_bulk_select) {
				$dropdown.find(".bulk-button").on('click', $.proxy(self._bulkSelect, self));
				$dropdown.find(".dialog-button").on('click', $.proxy(self._createBulkDialog, self));
			}

			// set dropdown states
			switch (self.options.selection_mode) {
				case 'single':
					$dropdown.addClass("single");
					break;
				case 'multiple':
					$dropdown.addClass("multiple");
					break;
			}

			// attach any event handlers for the dropdown
			$select.on("commondrop:updateDropdown", $.proxy(self.updateDropdown, self));
			$dropdown.on("mousewheel", $.proxy(self.onDropdownScroll, self));
			$dropdown.on('click', function(ev) { ev.stopPropagation(); });

			return $dropdown;
		},

		/**
		 * Destroy dropdown element for dropdown
		 *
		 * Destroys the dropdown element and remove it's
		 * reference from options
		 */
		_destroyDropdown: function() {
			var self = this;

			if(!self.options.$dropdown) return;

			// destroy the dropdown to save element space
			self.options.$dropdown.remove();
			self.options.$dropdown = null;
		},

		/**
		 * Create an option element
		 *
		 * Creates the DOM element for commondrop option, and binds
		 * events to the option element
		 *
		 * @param 	{Object} newOptionObj		A standard option object template
		 * @param 	{String} customClass		Optional custom class to be appended on the option div
		 *
		 * @return 	{element} 					The option element
		 */
		_createOption: function(newOptionObj, customClass) {
			var self = this;
			var $option = $("<div />").addClass("option");
			var $checkbox = $("<input type='checkbox'>").addClass("option-checkbox");
			var $label = $("<span />").addClass("option-label");
			var $optionModifier = $("<span />").addClass("option-modifier");

			if(customClass) $option.addClass(customClass);
			if(newOptionObj.groupId) $option.addClass("group-option");
			$option.attr("option-id", newOptionObj.id);
			$option.attr("group-id", newOptionObj.groupId);
			$option.attr("data-value", newOptionObj.value);
			$label.text(newOptionObj.text);
			$label.attr("title", newOptionObj.text);

			if (self.options.selection_mode == 'multiple') {
				$option.append($checkbox);
			}

			$option.append($label);

			if (self.options.enable_option_modifier) {
				var $a = $("<a />");
				$a.text(self.options.option_modifier.text);
				$optionModifier.append($a);
				$option.append($optionModifier);
				$optionModifier.on('click', $.proxy(self.onOptionModifierClick, this, self, $a));
			}

			// attach any event handlers for the display
			$option.on('mouseenter', $.proxy(self.setIndex, self));
			$option.on('click', $.proxy(self.onOptionClick, self));
			$checkbox.on('click', $.proxy(self.onOptionClick, self));

			return $option;
		},

		/**
		 * Create a group element
		 *
		 * Creates the DOM element for commondrop group header, and binds
		 * events to the group element
		 *
		 * @param 	{Object} newGroupObj		A standard group object template
		 *
		 * @return 	{element} 					The group header element
		 */
		_createGroup: function(newGroupObj) {
			var self = this;
			var $group = $("<div />").addClass("group");
			var $label = $("<span />").addClass("option-label");
			var $groupModifier = $("<span />").addClass("group-modifier");

			$group.attr("group-id", newGroupObj.id);
			$group.attr("data-value", newGroupObj.value);
			$label.text(newGroupObj.text);
			$group.append($label);

			if(self.options.enable_group_modifier) {
				var $a = $("<a />");
				$a.text(self.options.group_modifier.text);
				$groupModifier.append($a);
				$group.append($groupModifier);
				$groupModifier.on('click', $.proxy(self.onGroupModifierClick, this, self, $a));
			}

			return $group;
		},

		/**
		 * Toggle visibility of dropdown
		 *
		 * Open and closes dropdown by toggling class on display
		 */
		_toggleDropdown: function() {
			var self = this;
			var $display = self.options.$display;
			$display.toggleClass("open");
			if ($display.hasClass('open')) {
				self.open();
			} else {
				self.close();
			}
		},

		/**
		 * Toggle search position
		 *
		 * Depending on dropdown visibility, position the search in either the display
		 * or the dropdown to catch all inputs and focuses.
		 */
		repositionSearch: function() {
			var self = this;
			var $search = self.options.$search;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;
			var $searchBox = null;
			
			if(!$display) return; // needs display to act

			if($dropdown) $searchBox = self.options.$dropdown.find(".search-box");

			if($display.hasClass("open")) {
				$searchBox.prepend($search); // move search to dropdown and show
			} else {
				$display.append($search); // move search to display and hide
			}
		},

		/**
		 * Show the dropdown
		 *
		 * Opens the dropdown, repositions the dropdown in accordance
		 * to the display. Create the dropdown if it has not been created before.
		 * And perform first initialized state operations
		 */
		open: function() {
			var self = this;

			if(self.options.isDisabled) return;

			// trigger an event to close all other dropdown
			// forcing only one to be open at a time
			$(window).trigger("commondrop:close_other", {parent: self}); 

			// create the dropdown
			if(!self.options.$dropdown) {
				self.options.$dropdown = self._createDropdown();

				// append it to the DOM
				self.options.$display.after(self.options.$dropdown);
			}

			self.options.$display.addClass("open");
			self.options.$dropdown.addClass("open");
			
			self.updateDropdown(null, true); 	// true = prevent change trigger
			self.updateDisplay();
			self.updateFeatures();
			self.reposition();
			self.repositionSearch();
			self.setIndex();
			self.setColorStatus();

			// always focus on the search as it handles all index events
			// but wait until all other dropdowns have finished their close operation
			setTimeout(function() { 
				self.options.$search.focus(); 
			}, 50);	

			if(self.options.on_open) {
				self.options.on_open(self.options.$dropdown, self.options.$display, self.options.data);
			}
		},

		/**
		 * Hide the dropdown
		 *
		 * Hides the dropdown, reset search state if search is active, 
		 * and perform any close operations.
		 */
		close: function() {
			var self = this;
			var $dropdown = self.options.$dropdown;

			if (!self.options.$dropdown) return;
			if(!self.options.$dropdown.hasClass("open")) return;

			var $display = self.options.$display;
			var $input = self.options.$search;

			$display.removeClass("open");
			$dropdown.removeClass("open");

			// reset search by resetting option states
			self._search(true);
			self.options.isSearch = false;
			$input.val("");
			self.repositionSearch();
			// remove the dropdown
			if(self.options.destroy_on_close) self._destroyDropdown();

			if(self.options.on_close) self.options.on_close(self.options.$dropdown, self.options.$display, self.options.data);
		},

		/**
		 * Construct bulk select dialog
		 *
		 * Construct the bulk select dialog and attach any event handlers
		 * to the element. Showing/Hiding is handled in this function.
		 */
		_createBulkDialog: function() {
			var self = this;
			var $body = $("body");
			var $cover = $("<div />").addClass("commondrop-dialog-cover");
			var $searchInput = self.options.$search;

			var $dialog = $("<div><div class='header'></div><div class='content'></div><div class='results'></div><div class='footer'></div><div>").addClass("commondrop-dialog");
			var $input = $("<textarea>").addClass("bulk-input");
			
			var $selectItemsBtn = $("<div class='button primary'>Select Items</div>").addClass("select-button");
			var $cancelBtn = $("<div class='button'>Cancel</div>").addClass("cancel-button");
			var $continueBtn = $("<div class='button primary'>Continue</div>").addClass("continue-button");
			var $backBtn = $("<div class='button'>Back</div>").addClass("back-button");

			var bulkOutput = null;

			var onSelectClick = function() {
				bulkOutput = self._generateBulkOutput($input.val());
				$dialog.find(".results").empty().append(outputResultTable(bulkOutput));
				$dialog.removeClass("entry");
				$dialog.addClass("confirmation");
			};

			var onBackClick = function() {
				$dialog.removeClass("confirmation");
				$dialog.addClass("entry");
			};

			var onCancelClick = function() {
				$dialog.fadeOut(100, function() { $(this).remove(); });
				$cover.fadeOut(100, function() { $(this).remove(); });
			};

			var onContinueClick = function() {
				// copy input back into searchInput
				$searchInput.val($input.val());
				self._bulkSelect();	
				onCancelClick();
			};

			var outputResultTable = function() {
				var $table = $("<table />");
				var $total = $("<tr><td>Total items read</td><td>" + bulkOutput.total + "</td><tr>"); 
				var $read = $("<tr><td>Items to be selected</td><td>" + bulkOutput.select.length + "</td><tr>");
				var $found = $("<tr><td>Items to be unchanged</td><td>" + bulkOutput.unchange.length + "</td><tr>");
				var $notFound = $("<tr><td>Unknown Items</td><td>" + bulkOutput.unknown.length + "</td><tr>");
				var $notFoundList = $("<div />").addClass("not-found-list");

				$.each(bulkOutput.unknown, function(i, unknown) {
					$notFoundList.append("<div>- " + unknown + "</div>");
				});

				$table.append($total, $read, $found, $notFound, $notFoundList);
				return $table;
			};

			$selectItemsBtn.on("click", onSelectClick);
			$cancelBtn.on("click", onCancelClick);
			$continueBtn.on("click", onContinueClick);
			$backBtn.on("click", onBackClick);

			// copy search input into the dialog
			$input.val($searchInput.val());
			$dialog.find(".content").append($input);
			$dialog.find(".header").text("Bulk Multiple Select");
			$dialog.find(".footer").append($selectItemsBtn, $cancelBtn, $continueBtn, $backBtn);
			$body.append($cover);
			$body.append($dialog);

			$dialog.hide().fadeIn(300);

			// start off with entry
			$dialog.addClass("entry");

			// attach events
			$dialog.on('click', function(ev) { ev.stopPropagation(); });
			$cover.on('click', function(ev) { ev.stopPropagation(); });
		},


		/**
		 * Bulk select pasted options into the dropdwon
		 *
		 * Update the dropdown's data state by selecting options generated
		 * by the bulk_output. Upate the state of the dropdown after selection.
		 */
		_bulkSelect: function() {
			var self = this;
			var $input = self.options.$search;

			// only perform bulk action when there is an outputted difference
			// _generateBulkOutput must push results before _bulkSelect is called
			if(!self.options.bulk_outputs) return;

			$.each(self.options.bulk_outputs.select, function(i, id) {
				$.each(self.options.data, function(i, option) {
					if(option.id == id) {
						option.selected = true;
						return false; // fast exit
					}
				});
			});

			// back to search mode
			self._toggleBulkStatus(false);
			$input.val("");

			self.updateDropdown();
			self.updateDisplay();
			self.updateFeatures();
		},

		/**
		 * Show/Hide/Update bulk select status row
		 *
		 * Based on bulk output (selected/unselected/unknown), show or hide or update
		 * the bulk output status row and display bulk status to the user
		 *
		 * @param 	{Boolean} mode 		True/False value indicating bulk status visibility
		 */
		_toggleBulkStatus: function(mode) {
			var self = this;
			var $input = self.options.$search;
			var $bulkBox = self.options.$dropdown.find(".bulk-box");
			var $selectSpan = $bulkBox.find(".select");
			var $unchangeSpan = $bulkBox.find(".unchange");
			var $unknownSpan = $bulkBox.find(".unknown");

			if(mode === true) {
				// first genreate bulk outputs 
				self.options.bulk_outputs = self._generateBulkOutput($input.val());
				if(self.options.bulk_outputs.select.length) {
					$selectSpan.html("<p>" + self.options.bulk_outputs.select.length + "</p>" + " select").stop().fadeIn(100);
				} else {
					$selectSpan.text("").hide();
				}
				if(self.options.bulk_outputs.unchange.length) {
					$unchangeSpan.html("<p>" + self.options.bulk_outputs.unchange.length + "</p>" + " no change").stop().fadeIn(100);	
				} else {
					$unchangeSpan.text("").hide();
				}
				if(self.options.bulk_outputs.unknown.length) {
					$unknownSpan.html("<p>" + self.options.bulk_outputs.unknown.length + "</p>" + " unknown").stop().fadeIn(100);
				} else {
					$unknownSpan.text("").hide();
				}
				
				self.options.$dropdown.addClass("bulk-active");

			} else if(mode === false) {
				self.options.$dropdown.removeClass("bulk-active");				
			}
		},

		/**
		 * Generate bulk output status prior to bulk action
		 *
		 * Generate a difference object of what is to be selected, unchanged, and unknown
		 * options from the pasted bulk input (from the search input) to be used by bulk select.
		 *
		 * @param 	{string} str		the full input string to be delimitized
		 *
		 * @return 	{Object} 			the generated difference object
		 */
		_generateBulkOutput: function(str) {
			var self = this;	
			var delimiter = ",";
			var tokens = [];
			var triggerWord = self.options.bulk_select_trigger_word;
			var values = [];

			var to_be_selected = []; // option ID
			var to_be_unchanged = []; // option ID
			var unknown = []; // option values

			// remove leading trigger word
			if(str === triggerWord) {
				tokens = [];	
			} else {
				str = str.substring(triggerWord.length, str.length);
				tokens = str.split(delimiter);
			}

			$.each(tokens, function(i, token) {
				if(token.trim()) values.push(token.trim());
			});

			$.each(values, function(i, value) {
				var found = false;
				// find corresponding text value in the list of values
				$.each(self.options.data, function(i, option) {
					if(option.text.toLowerCase() == value.toLowerCase()) { // CASE NON-SENSITIVE
						found = true;
						if(option.selected) {
							to_be_unchanged.push(option.id);
						} else {
							to_be_selected.push(option.id);
						}
						return false; // exit early
					}
				});
				if(!found) unknown.push(value);
			});

			return {
				total: to_be_selected.length + to_be_unchanged.length + unknown.length,
				select: to_be_selected,
				unchange: to_be_unchanged,
				unknown: unknown
			};
		},


		/**
		 * Performs search on the list of options
		 *
		 * Searches the list of options by toggling render property of the option object.
		 * Search is performed asynchronously with a delay. Search performs string comparisons
		 * on the option's text. Dropdown state is updated after search has concluded.
		 * 
		 *
		 * @param 	{Boolean} reset		Flag indicating if search should end immediately
		 */
		_search: function(reset) {
			var self = this;
			var $input = self.options.$search;
			var searchTerm = $input.val();
			var data = self.options.data;
			var searchTimingMode = searchTerm ? "set" : "quickset";

			self.toggleSearchTimer("clear");
			self.toggleSearchTimer(searchTimingMode, function() {

				// first search options within 
				if(self.options.enable_groups) {
					$.each(data, function(i, group) {

						// count the number of hidden items in the group, if all are hidden,
						// hide the group header as well
						var groupOptionHiddenCount = 0;

						$.each(group.data, function(j, option) {
							// reset / nothing in input / substring found
							if((reset) || (!searchTerm) || (option.text.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1)) {
								option.render = true;
							} else {
								option.render = false;
								groupOptionHiddenCount++;
							}
						});
						if(groupOptionHiddenCount == group.data.length) {
							group.render = false;
						} else {
							group.render = true;
						}
					});	
				} else {
					$.each(data, function(i, option) {
						// reset / nothing in input / substring found
						if((reset) || (!searchTerm) || (option.text.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1)) {
							option.render = true;
						} else {
							option.render = false;
						}
					});
				}

				self.updateDropdown(null, true);
				self.updateFeatures();
			});

		},

		/**
		 * Re-render the options based on differences from the options in data vs the options in UI
		 *
		 * Commondrop's options are changed and mutated through this function only. Options are collected
		 * in the form of the data set (stored in the widget data), and their element countertype, in the
		 * DOM. updateDropdown will perform addition, removal, and sorting of the options in order to sync
		 * the difference between the data and the UI. Options are first added to the DOM where it did
		 * not exist before in DOM, then options that cease to exist in data are removed from the DOM. Finally
		 * options themselves are updated sequentially to match their data states (selected/hidden/render).
		 *
		 * An expedited process to target a single option's state can be performed when changeObj is provided.
		 *
		 * @param 	{String} 	changeObj			Given an option ID and action type in object form, 
		 											add/remova steps are skipped, and update procedure is 
		 											performed only for that option

		 * @param 	{Boolean} 	preventChange		Flag preventing change event from triggering on the origianl select
		 * @param 	{Boolean} 	preventSort			Flag preventing sort operations
		 * @param 	{Array} 	customData			An array of options data to perform updateDropdown on specifically
		 * @param 	{String} 	optionClass			A specific class to select options with when gathering DOM options
		 */
		updateDropdown: function(changeObj, preventChange, preventSort, customData, optionClass) {
			var self = this;

			if(!self.options.$dropdown) return;
			
			/* VARIABLES ========================================== */

			var $dropdown = self.options.$dropdown;
			var data = customData ? customData : self.options.data;
			var $optionsBox = $dropdown.find(".options-box");
			var $groups = $optionsBox.find("div.group");
			var totalRenderedOptions = 0; // a count of the total number of options in the data set

			/* LOCAL FUNCTIONS ========================================== */

			var findMatchingDataObj = function(id, localizedData) {
				var matchingOption = null;
				$.each(localizedData, function(i, dataObj) {
					if (dataObj.id == id) {
						matchingOption = dataObj;
						return false;
					}
				});
				return matchingOption;
			};
			var findMatchingGroupObj = function(id) {
				var matchingGroup = null;
				$.each(data, function(i, groupObj) {
					if (groupObj.id == id) {
						matchingGroup = groupObj;
						return false;
					}
				});
				return matchingGroup;
			};
			var findMatchingOptionEl = function(id, $options) {
				return $options.filter("[option-id=" + id + "]");
			};
			var findMatchingGroupEl = function(id) {
				return $groups.filter("[group-id=" + id + "]");
			};

			var findOptionsElByGroup = function(id, $options) {
				return $options.filter("[group-id=" + id + "]");
			};

			// NOTE ** context of data has changed here, it's either the self.options.data array, or the data array within a group
			var updateOptions = function(data, $groupEl) {

				var $options = null;
				var $sortedOptions = null;
				var dataIds = []; // array of option IDs in the set
				var elIds = []; // array of option IDs in the element
				var addedIds = [];
				var removedIds = [];

				// retrieve IDs from both the set and the element
				$.each(data, function(i, dataObj) {
					// dataObj must have rendered set to true in order to be considered rendered
					// ** TEMP disabled for single buffer
					if(dataObj.render) {
						dataIds.push(dataObj.id);
						// count rendered data options towards the final count
						totalRenderedOptions++;
					}
				});

				// get the context of $options
				if($groupEl) {
					$options = $groupEl.nextUntil(".group");
				} else {
					$options = optionClass ? $optionsBox.find(".option." + optionClass) : $optionsBox.find(".option:not(.group-option)");
				}

				$options.each(function() {
					if(optionClass) {
						if($(this).hasClass(optionClass)) {
							elIds.push($(this).attr("option-id"));
						}
					} else {
						elIds.push($(this).attr("option-id"));
					}
				});

				// get difference and retrieve what's been added to the state vs what's been removed from the state
				addedIds = $(dataIds).not(elIds).toArray();
				removedIds = $(elIds).not(dataIds).toArray();

				// skip add/remove procedures if changeObj is provided to expedite processes
				if(!changeObj) {
					
					// removal ------------------
					// remove any options not in the data set from the UI
					$.each(removedIds, function(i, id) {
						findMatchingOptionEl(id, $options).remove();
					});

					// add ----------------------
					// add any options not in the data set into the UI
					if($groupEl) addedIds.reverse(); // insert them in reverse, since after() pushes elements from front
					$.each(addedIds, function(i, id) {
						var $newOption = self._createOption(findMatchingDataObj(id, data), optionClass);
						if($groupEl) {
							$groupEl.after($newOption);
						} else {
							$optionsBox.append($newOption);
						}
					});
				}

				// options needs to be re-selected by jQuery since we could've appended more before
				// get the context of $options
				if($groupEl) {
					$options = $groupEl.nextUntil(".group");
				} else {
					$options = optionClass ? $optionsBox.find(".option." + optionClass) : $optionsBox.find(".option:not(.group-option)");
				}

				// change -------------------
				// change any option's state based on the data set array
				// if changeObj is provided, target that option specifically
				if (changeObj) {
					var optionObj = findMatchingDataObj(changeObj.id, data);
					var optionEl = findMatchingOptionEl(changeObj.id, $options);
					if(optionObj) self.setOptionUI(optionEl, optionObj);
				} else {
					$.each(data, function(i, dataObj) {
						var optionEl = findMatchingOptionEl(dataObj.id, $options);
						if(dataObj) self.setOptionUI(optionEl, dataObj);
					});
				}

				// sort ---------------------
				// reflect the sort order of the data set array with the UI
				// *NOTE only perform pin_selected actions on intial change (changeObj: null)

				if(!preventSort && data.length) { // only sort when there is a set of options
					// sort the set
					$.each(data, function(i, dataObj) { dataObj.order = i; });
					$sortedOptions = $options.sort(function(optionA, optionB) {
						var aData = findMatchingDataObj(optionA.getAttribute("option-id"), data);
						var bData = findMatchingDataObj(optionB.getAttribute("option-id"), data);
						if(aData.order > bData.order) return 1;
						if(aData.order < bData.order) return -1;
						return 0;
					});
					
					if($groupEl) {
						$groupEl.after($sortedOptions);
					} else {
						$sortedOptions.appendTo($optionsBox);
					}

					// separate selected and unselected ** Disabled for groups
					if(self.options.pin_selected) {
						var $selected = [];
						var $unselected = [];
						// remove all option states
						$options.removeClass("last");
						$options.each(function() {
							var optionObj = findMatchingDataObj($(this).attr("option-id"), data);
							if(optionObj.selected) {
								$selected.push($(this));
							} else {
								$unselected.push($(this));
							}
						});

						// add a last class to the end of each section
						if($selected.length) $selected[$selected.length - 1].addClass("last");

						// appned won't add more options, since the option
						// is a pointer, append will simply move the option to it's
						// new position
						$optionsBox.append($selected);
						$optionsBox.append($unselected);
					}
				}
			};

			/* UPDATE START ========================================== */

			if(self.options.enable_groups) {
				var groupIds = [];
				var groupElIds = [];
				var addedGroupIds = [];
				var removedGroupIds = [];

				// retrieve IDs from both the group data set and the elements
				$.each(data, function(i, groupObj) {
					if(groupObj.render) groupIds.push(groupObj.id);
				});

				$groups.each(function() {
					groupElIds.push($(this).attr("group-id"));
				});

				// get difference and retrieve what's been added to the state vs what's been removed from the state
				addedGroupIds = $(groupIds).not(groupElIds).toArray();
				removedGroupIds = $(groupElIds).not(groupIds).toArray();

				// group removal ------------------
				// remove any groups not in the data set from the UI
				$.each(removedGroupIds, function(i, id) {
					findMatchingGroupEl(id).remove();
					// also remove any options associated with that group
					findOptionsElByGroup(id, $optionsBox.find(".option")).remove();
				});

				// group add ----------------------
				// add any groups not in the data set into the UI
				$.each(addedGroupIds, function(i, id) {
					$optionsBox.append(self._createGroup(findMatchingGroupObj(id)));
				});

				// groups needs to be re-selected by jQuery since we could've appended more before
				$groups = self.options.$dropdown.find("div.group");

				// change -------------------
				// change any option's state based on the data set array
				// if changeObj is provided, target that option specifically{
				$.each(data, function(i, groupObj) {
					var groupEl = findMatchingGroupEl(groupObj.id, $groups);
					if(groupEl) self.setGroupUI(groupEl, groupObj);
				});
				
				$groups.each(function() {
					var groupId = $(this).attr("group-id");
					var matchingGroup = findMatchingGroupObj(groupId);
					if(matchingGroup) {
						updateOptions(matchingGroup.data, $(this));
					}
				});
			} else {
				// update ALL the data with new states
				updateOptions(data);
			}

			// if no options exist, show the empty box
			if(!totalRenderedOptions) {
				if(self.options.isSearch) {
					if(!self.options.isLoading) {
						self._createEmptyResult();
					}	
				} else {
					self._createEmpty();
				}
			} else {
				self._destroyEmptyResult();
				self._destroyEmpty();
			}
	
			// any changes made to the dropdown is reflected back in the vanilla select element
			self.setSelect();
			self.triggerChange(preventChange, changeObj);
			self.options.$search.focus();
		},

		triggerChange: function(preventOverride, changeObj) {
			var self = this;
			var data = self.options.data;
			var matchingOption = null;

			if(self.options.trigger_change && !preventOverride && self.options.initially_loaded) {

				// if a changeObj is provided, it indicates that a particular option has been
				// changed exclusively. We can let the user know exactly which option changed
				// by providing the changed value.
				if(changeObj) {
					if(self.options.enable_groups) {
						$.each(data, function(i, groupObj) {
							$.each(groupObj.data, function(i, dataObj) {
								if (dataObj.id == changeObj.id) {
									matchingOption = dataObj;
									return false;
								}	
							});
						});	
					} else {
						$.each(data, function(i, dataObj) {
							if (dataObj.id == changeObj.id) {
								matchingOption = dataObj;
								return false;
							}
						});
					}

					self.options.$select.trigger("change", {
						option: matchingOption,
						action: changeObj.action
					});
				} else {
					self.options.$select.trigger("change");
				}
			}
		},

		/**
		 * Set option properties
		 *
		 * Given a pair of option element and option data object, update the option element with
		 * the same properties of the data by changing their class or adding attributes.
		 *
		 * @param 	{element} 	optionEl			The option element to change in the dropdown
		 * @param 	{Object} 	optionObj			The option data object to refer from in the data set
		 */
		setOptionUI: function(optionEl, optionObj) {
			var self = this;
			var $dropdown = self.options.$dropdown;
			optionEl.find(".option-label").text(optionObj.text);
			optionEl.attr("data-value", optionObj.value);
			optionEl.find("input").prop("checked", optionObj.selected);
			optionEl.find("input").prop("disabled", optionObj.disabled);

			if(optionObj.selected) {
				optionEl.addClass("selected");
			} else {
				optionEl.removeClass("selected");
			}
			if(optionObj.hidden) {
				optionEl.addClass("hidden");
			} else {
				optionEl.removeClass("hidden");
			}
			if(optionObj.disabled) {
				optionEl.addClass("disabled");
			} else {
				optionEl.removeClass("disabled");
			}
			
			// if the option has an associated group, hide the option independent of the
			// hidden property. This is to ensure that when the group is unhidden, the 
			// options still retain their independent hidden status
			if(self.options.enable_groups) {
				var $associatedGroup = null;
				$associatedGroup = $dropdown.find(".group[group-id=" + optionObj.groupId + "]");

				if($associatedGroup && $associatedGroup.hasClass("hidden")) {
					optionEl.addClass("group-hidden");
				} else {
					optionEl.removeClass("group-hidden");
				}

				if($associatedGroup && $associatedGroup.hasClass("disabled")) {
					optionEl.addClass("group-disabled");
				} else {
					optionEl.removeClass("group-disabled");
				}
			}
		},

		/**
		 * Set group header properties
		 *
		 * Given a pair of group element and group data object, update the group element with
		 * the same properties of the data by changing their class or adding attributes.
		 *
		 * @param 	{element} 	groupEl				The group element to change in the dropdown
		 * @param 	{Object} 	groupObj			The group data object to refer from in the data set
		 */
		setGroupUI: function(groupEl, groupObj) {
			groupEl.find(".option-label").text(groupObj.text);
			groupEl.attr("data-value", groupObj.value);

			if(groupObj.hidden) {
				groupEl.addClass("hidden");
			} else {
				groupEl.removeClass("hidden");
			}

			if(groupObj.disabled) {
				groupEl.addClass("disabled");
			} else {
				groupEl.removeClass("disabled");
			}
		},

		/**
		 * Update the original option elemnt
		 *
		 * Given a pair of group element and group data object, update the group element with
		 * the same properties of the data by changing their class or adding attributes.
		 *
		 * @param 	{element} 	groupEl				The group element to change in the dropdown
		 * @param 	{Object} 	groupObj			The group data object to refer from in the data set
		 */
		setOptionElData: function(optionEl, optionObj) {
			if(optionObj.selected) {
				optionEl.prop("selected", true);
			} else {
				optionEl.prop("selected", false);
			}
			if(optionObj.disabled) {
				optionEl.prop("disabled", true);
			} else {
				optionEl.prop("disabled", false);
			}
		},

		/**
		 * Create an option element
		 *
		 * Given a option object, create an option object with it's properties
		 *
		 * @param 	{Object} 	optionObj			The option data object to refer from in the data set
		 */
		createOptionEl: function(optionObj) {
			var $option = $("<option></option>");
			$option.prop("selected", optionObj.selected);
			$option.prop("disabled", optionObj.disabled);

			// placeholder options should be completely empty
			if(!optionObj.isPlaceholder) {
				$option.text(optionObj.text);
				$option.val(optionObj.value);
			}

			return $option;
		},

		/**
		 * Set a colored border depending on a given status
		 *
		 * Given a status, surround the dropdown and display with a colored border. If a status argument
		 * is provided, update commondrop's internal color status and change the border. If status is null
		 * set the status based on the internal color status in options.
		 *
		 * @param 	{string} 	status			The intended color status
		 *
		 */
		setColorStatus: function(status) {
			var self = this;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;

			if(status === undefined) {
				status = self.options.color_status;
			}

			switch(status) {
				case 'error':
					if($display) $display.addClass("error-status");
					if($dropdown) $dropdown.addClass("error-status");
					self.options.color_status = 'error';
					break;
				
				case 'warning':
					if($display) $display.addClass("warning-status");
					if($dropdown) $dropdown.addClass("warning-status");
					self.options.color_status = 'warning';
					break;

				case 'success':
					if($display) $display.addClass("success-status");
					if($dropdown) $dropdown.addClass("success-status");
					self.options.color_status = 'success';
					break;

				default:
					if($display) $display.removeClass("error-status warning-status error-status");
					if($dropdown) $dropdown.removeClass("error-status warning-status error-status");
					self.options.color_status = null;
					break;
			}
		},	

		/**
		 * Set original select's options
		 *
		 * Clear the original select and append option elements back into the original 
		 * select after changes have been made to the data set such that the original select
		 * can be accessed for a list of selected values. If keep_option is false, 
		 * only selected options are created, otherwise, all options and their states are reflected
		 * back into the select element.
		 */
		setSelect: function() {
			var self = this;
			var data = self.options.data;
			var $select = self.options.$select;
			var $options = $select.find("option");
			var $optGroups = $select.find("optgroup");

			// first remove all options
			$options.remove();
			$optGroups.remove();
			if(self.options.enable_groups) {
				// then append back only selected options
				$.each(data, function(i, groupObj) {
					$.each(groupObj.data, function(i, dataObj) {
						if(self.options.keep_options) {
							// append back every option
							$select.append(self.createOptionEl(dataObj));
						} else {
							// only append selected options
							if(dataObj.selected) $select.append(self.createOptionEl(dataObj));
						}
					});
				});	
			} else {
				// then append back only selected options
				$.each(data, function(i, dataObj) {
					if(self.options.keep_options) {
						// append back every option
						$select.append(self.createOptionEl(dataObj));
					} else {
						// only append selected options
						if(dataObj.selected) $select.append(self.createOptionEl(dataObj));
					}
				});	
			}
		},

		/**
		 * Calculate and reposition the widget
		 *
		 * Calculate the offset of both display and dropdown and move the widget elements
		 * into their correct position. Also perform any callbacks after reposition.
		 */
		reposition: function() {
			var self = this;
			var $select = self.options.$select;
			var $display = self.options.$display;
			var topOffset = 1;
			var padding = 0;
			var width = null;

			if(self.options.inherit_width) {
				width = $select.width();
			} else {
				width = self.options.width;
			}

			if (self.options.$display) {
				self.options.$display.css({
					position: 'relative',
					display: 'inline-block',
					width: width
				});
			}

			if (self.options.$dropdown) {
				// reposition the dropdown to the display
				self.options.$dropdown.css({
					position: 'absolute',
					top: $display.position().top + $display.height() + topOffset,
					left: $display.position().left,
					width: $display.width() + padding // 10 for padding of display
				});
			}

			if(self.options.on_reposition) self.options.on_reposition(self.options.$dropdown, self.options.$display);
		},

		/**
		 * Update any optional features' states
		 *
		 * Update feature states and views (e.g. status, modifiers, action button)
		 * based on the data set when called.
		 */
		updateFeatures: function() {
			var self = this;
			var total = 0;
			var selected = 0;
			var data = self.options.data;
			var $dropdown = self.options.$dropdown;


			if(!$dropdown) return;
			if(!self.options.enable_status) return;

			// update status
			if(self.options.enable_groups) {
				$.each(data, function(i, group) {
					$.each(group.data, function(j, obj) {
						if (obj.render && obj.selected) selected++;
						if(obj.render) total++;
					});
				});
			} else {
				$.each(data, function(i, obj) {
					if (obj.render && obj.selected) selected++;
					if(obj.render) total++;
				});
			}
			$dropdown.find(".status").text(selected + " / " + total);

			// calculate width for modifiers in accordance to dropdown width
			if(self.options.enable_modifiers) {
				var $modifierBox = $dropdown.find(".modifiers");
				var totalModifiers = self.options.modifiers.length;
				var $modifiers = $modifierBox.find(".modifier");
				var margin = 10;
				var scrollPadding = 30;
				var eachModifierWidth = (($modifierBox.width() - scrollPadding) / totalModifiers) - margin;

				$modifiers.each(function() { $(this).width(eachModifierWidth); });
			}
		},

		/**
		 * Re-render the display component
		 *
		 * Based on display mode and type, render and reposition the display depending
		 * on the selected options and attach handlers to elements such as tags.
		 *
		 * @param 	{Array} 	customData			A custom set of options to get selected options from
		 */
		updateDisplay: function(customData) {
			var self = this;
			var $display = self.options.$display;
			var selected = [];
			var total = 0;
			var data = customData ? customData : self.options.data;

			if(!$display) return;

			// gather all options
			if(self.options.enable_groups) {
				$.each(data, function(i, group) {
					total = total + group.data.length;
					$.each(group.data, function(j, dataObj) {
						if(dataObj.selected) selected.push(dataObj);
					});
				});
			} else {
				$.each(data, function(i, dataObj) {
					if(dataObj.selected) selected.push(dataObj);
				});
			}

			if (selected.length) {
				if (self.options.selection_mode == 'single') {
					// special case - show single_placeholder_text instead of the option text
					if(selected.length == 1 && selected[0].isPlaceholder) {
						$display.find("span").text(self.options.single_placeholder_text);
					} else {
						$display.find("span").text(selected[0].text);
					}
				}
				if (self.options.selection_mode == 'multiple') {
					if(self.options.display_mode == 'tags') {
						$display.find("span").hide();
						$display.find(".tag").remove();
						$.each(selected, function(i, dataObj) {
							var $tag = $("<div />").addClass("tag").text(dataObj.text);
							var $tagRemove = $("<span />").addClass("tag-remove");
							$tag.attr("option-id", dataObj.id);
							if(self.options.enable_groups) $tag.attr("group-id", dataObj.groupId);
							$tag.append($tagRemove);
							$display.append($tag);

							// attach any event handlers for the tags
							$tagRemove.on('click', $.proxy(self.onTagClick, self));
						});
					}
					if(self.options.display_mode == 'count') {
						$display.find("span").text(selected.length + " items selected");	
					}
				}
			} else {
				if (self.options.selection_mode == 'single') {
					// in single selection mode, an option must be selected, thus this
					// scenario will never be reached
					$display.find("span").text(self.options.single_placeholder_text);
				}
				if (self.options.selection_mode == 'multiple') {
					if(self.options.display_mode == 'tags') {
						$display.find("span").show();
						$display.find(".tag").remove();
					}
					if(self.options.display_mode == 'count') {
						$display.find("span").text(self.options.multiple_placeholder_text);
					}
				}
			}

			self.reposition();
		},

		/**
		 * Lock/Unlock dropdown component
		 *
		 * Lock the dropdown by covering dropdown with an element, preventing user
		 * interaction when locked, and vise-versa.
		 *
		 * @param 	{Boolean} 	mode			Flag indicating lock or unlock operation
		 */
		lockDropdown: function(mode) {
			var self = this;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;
			
			if(mode === true) {
				if($display) $display.find(".cover-box").addClass("active");
				if($dropdown) $dropdown.find(".cover-box").addClass("active");
			}

			if(mode === false) {
				if($display) $display.find(".cover-box").removeClass("active");
				if($dropdown) $dropdown.find(".cover-box").removeClass("active");
			}	
		},

		/**
		 * Event handler for dropdown click
		 *
		 * Event handler for clicking anywhere on the dropdown.
		 * Event bubbling is prevented as default.
		 *
		 * @param 	{Object} 	e				The jQuery event object
		 */
		onDropdownClick: function(e) {
			e.stopPropagation();
		},

		/**
		 * Event handler for dropdown scroll
		 *
		 * Event handler for scrolling anywhere on the dropdown.
		 * Event bubbling is prevented as default.
		 *
		 * @param 	{Object} 	e				The jQuery event object
		 */
		onDropdownScroll: function(e) {
			e.stopPropagation();
		},

		/**
		 * Event handler for display tag click
		 *
		 * Event handler for clicking on an option tag on the display.
		 * Event bubbling is prevented as default.
		 *
		 * @param 	{Object} 	e				The jQuery event object
		 */
		onTagClick: function(ev) {
			var self = this;
			// stop any default action and bubbling from occurring in UI
			// all UI updates are handled through updateDropdown
			ev.stopPropagation();
			var $tag = $(ev.target).closest(".tag");
			var matchingOption = null;

			// find the option in set
			if(self.options.enable_groups) {
				var groupId = $tag.attr('group-id');
				var matchingGroup = null;
				// first find the group
				$.each(self.options.data, function(i, group) {
					if(group.id == groupId) matchingGroup = group;
				});
				// then find the option
				$.each(matchingGroup.data, function(i, option) {
					if (option.id == $tag.attr("option-id")) matchingOption = option;
				});
			} else {
				$.each(self.options.data, function(i, option) {
					if (option.id == $tag.attr("option-id")) matchingOption = option;
				});
			}

			matchingOption.selected = false;
			
			// update UIs
			if(self.options.$dropdown) {
				self.updateDropdown({
					id: matchingOption.id,
					action: 'deselect'
				}, null, true); // true == prevent sorting
				self.updateFeatures();
			}
			self.updateDisplay();
		},

		/**
		 * Event handler for dropdown option click
		 *
		 * Event handler for clicking on an option in the dropdown. Selection
		 * of option starts here.
		 *
		 * @param 	{Object} 	e				The jQuery event object
		 */
		onOptionClick: function(ev) {
			// stop any default action and bubbling from occurring in UI
			// all UI updates are handled through updateDropdown
			ev.stopPropagation();

			var self = this;
			var $option = $(ev.target).is("div") ? $(ev.target) : $(ev.target).closest("div");
			var isGroupOption = $option.hasClass("group-option");
			var matchingOption = null;
			// find the option in set
			if(isGroupOption) {
				var groupId = $option.attr('group-id');
				var matchingGroup = null;
				// first find the group
				$.each(self.options.data, function(i, group) {
					if(group.id == groupId) matchingGroup = group;
				});
				// then find the option
				$.each(matchingGroup.data, function(i, option) {
					if (option.id == $option.attr("option-id")) matchingOption = option;
				});
			} else {
				$.each(self.options.data, function(i, option) {
					if (option.id == $option.attr("option-id")) matchingOption = option;
				});
			}
			
			// if option is disabled, do nothing
			if (matchingOption.disabled) return;

			if (self.options.selection_mode == 'single') {
				// set all to false, except the selected
				if(self.options.enable_groups) {
					$.each(self.options.data, function(i, group) {
						$.each(group.data, function(j, dataObj) {
							dataObj.selected = false;
						});
					});
				} else {
					$.each(self.options.data, function(i, option) {
						option.selected = false;
					});
				}
				
				// select the clicked option
				matchingOption.selected = true;
			}

			if (self.options.selection_mode == 'multiple') {
				// toggle selection and update state
				matchingOption.selected = !matchingOption.selected;
			}

			// update UIs
			self.updateDropdown({
				id: matchingOption.id, 
				action: matchingOption.selected ? 'select' : 'deselect' // use final selected state as action
			}, null, true); // true == prevent sort
			self.updateDisplay();
			self.updateFeatures();

			if (self.options.selection_mode == 'single') {
				self._toggleDropdown();
			}
		},

		/**
		 * Event handler for group modifier
		 *
		 * Event handler for clicking on a group modifier on group header.
		 * Group modifier is mapped to the action provided in the group_modifier
		 * array.
		 *
		 * @param 	{Object} 	self			The widget context this
		 * @param 	{Element} 	el				The modifier span element
		 * @param 	{Object} 	ev				The jQuery event object
		 */
		onGroupModifierClick: function(self, el, ev) {
			// stop any default action and bubbling from occurring in UI
			// all UI updates are handled through updateDropdown
			ev.stopPropagation();
			ev.preventDefault();

			var groupModifier = self.options.group_modifier;
			var $selectedGroup = el.closest(".group");

			// perform the action provided by the modifier
			if(groupModifier && groupModifier.action) {
				groupModifier.action(self.options.data, $selectedGroup);
			}

			// update the dropdown to reflect state change
			self.updateDropdown(null, null, true); // prevent order change
			self.updateDisplay();
			self.updateFeatures();
		},

		/**
		 * Event handler for option modifier
		 *
		 * Event handler for clicking on a modifier on an option.
		 * Option modifier is mapped to the action provided in the option_modifier
		 * array.
		 *
		 * @param 	{Object} 	self			The widget context this
		 * @param 	{Element} 	el				The modifier span element
		 * @param 	{Object} 	ev				The jQuery event object
		 */
		onOptionModifierClick: function(self, el, ev) {
			// stop any default action and bubbling from occurring in UI
			// all UI updates are handled through updateDropdown
			ev.stopPropagation();
			ev.preventDefault();

			var optionModifier = self.options.option_modifier;
			var $selectedOption = el.closest(".option");

			// perform the action provided by the modifier
			if(optionModifier && optionModifier.action) {
				optionModifier.action(self.options.data, $selectedOption, self.options.enable_groups);
			}

			// update the dropdown to reflect state change
			self.updateDropdown(null, null, true); // prevent order change
			self.updateDisplay();
			self.updateFeatures();
		},

		/**
		 * Event handler for global modifier
		 *
		 * Event handler for clicking on a global on the dropdown.
		 * Global modifier is mapped to the action provided in the modifier
		 * array.
		 *
		 * @param 	{Object} 	self			The widget context this
		 * @param 	{Element} 	el				The modifier span element
		 * @param 	{Object} 	ev				The jQuery event object
		 */
		onModifierClick: function(self, el) {
			
			var matchingModifier = null;
			var id = el.attr("data-id");

			// find the modifier by id
			$.each(self.options.modifiers, function(i, modifier) {
				if(modifier.id == id) {
					matchingModifier = modifier;
					return false;
				}
			});

			// perform the action provided by the modifier
			if(matchingModifier && matchingModifier.action) {
				matchingModifier.action(self.options.data, el, self.options.enable_groups);
			}

			// then update the dropdown
			self.updateDropdown(null, null, true); // prevent order change
			self.updateDisplay();
			self.updateFeatures();
		},

		/**
		 * Event handler for search icon in search input
		 *
		 * Event handler for clicking on the search icon right of the search input.
		 *
		 * @param 	{Object} 	self			The widget context this
		 * @param 	{Element} 	el				The search icon element
		 */
		onSearchIconClick: function(ev) {
			var self = this;
			var $searchIcon = $(ev.target);
			var $searchInput = self.options.$search;
			if(!$(ev.target).hasClass("active")) return;

			$searchIcon.removeClass("active");
			$searchInput.val("");
			$searchInput.focus();
			self._search(true);
		},

		/**
		 * Highlight next available option upwards
		 *
		 * Calculate the next viable option to highlight in the dropdown options
		 * and move the current highlighted index. Scroll the list automatically
		 * if the highlighted option is out of view.
		 *
		 * @param 	{Object} 	ev				The jQuery event object
		 */
		upIndex: function() {
			var self = this;
			var index = self.options.index;
			var $dropdown = self.options.$dropdown;
			var $optionsBox = $dropdown.find(".options-box");
			var $options = $optionsBox.find(".option");
			var $highlighted = $options.filter(".highlighted");
			var $indexOption = $($options[index]);
			var $nextIndexOption = $indexOption;
			var optionHeight = 30;

			// recursively search for the next available option that
			// is not hidden or a group header. If it cannot be found, keep index the same
			do {
				$nextIndexOption = $nextIndexOption.prev();
			}
			while(($nextIndexOption.length != 0) && ($nextIndexOption.hasClass("hidden") || $nextIndexOption.hasClass("group")));
			// only set index if the next index is found
			if($nextIndexOption.length != 0) self.options.index = $options.index($nextIndexOption);

			// set style
			$highlighted.removeClass("highlighted");
			$($options[self.options.index]).addClass("highlighted");

			// keep highlight on screen by force scrolling when highlight is out of the scroll view
			if($($options[self.options.index]).position().top <= 0) {
				$optionsBox.scrollTop($optionsBox.scrollTop() - optionHeight);
			}
		},

		/**
		 * Highlight next available option downwards
		 *
		 * Calculate the next viable option to highlight in the dropdown options
		 * and move the current highlighted index. Scroll the list automatically
		 * if the highlighted option is out of view.
		 *
		 * @param 	{Object} 	ev				The jQuery event object
		 */
		downIndex: function() {
			var self = this;
			var index = self.options.index;
			var $dropdown = self.options.$dropdown;
			var $optionsBox = $dropdown.find(".options-box");
			var $options = $optionsBox.find(".option");
			var $highlighted = $options.filter(".highlighted");
			var $indexOption = $($options[index]);
			var $nextIndexOption = $indexOption;
			var dropdownHeight = $optionsBox.height();
			var optionHeight = 30;

			// recursively search for the next available option that
			// is not hidden or a group header. If it cannot be found, keep index the same
			do {
				$nextIndexOption = $nextIndexOption.next();
			}
			while(($nextIndexOption.length != 0) && ($nextIndexOption.hasClass("hidden") || $nextIndexOption.hasClass("group")));
			// only set index if the next index is found
			if($nextIndexOption.length != 0) self.options.index = $options.index($nextIndexOption);

			$highlighted.removeClass("highlighted");
			$($options[self.options.index]).addClass("highlighted");

			// keep highlight on screen by force scrolling when highlight is out of the scroll view
			if($($options[self.options.index]).position().top + optionHeight > dropdownHeight) {
				$optionsBox.scrollTop($optionsBox.scrollTop() + optionHeight);
			}

		},

		/**
		 * Highlight an option by index
		 *
		 * Highlight the option by index in the dropdown options and move the current 
		 * highlighted index directly. Scroll the list automatically if the highlighted 
		 * option is out of view. If ev is provided, it's origin is a hover event on the option.
		 * If ev is undefined, set the highlighted option to the saved index. If ev is a number,
		 * set the highlight to the indexed option.
		 *
		 * @param 	{(Object|Integer)} 	param		The jQuery event object, the index of option to select
		 */
		setIndex: function(param) {
			var self = this;
			var $dropdown = self.options.$dropdown;
			var $optionsBox = $dropdown.find(".options-box");
			var $options = $optionsBox.find(".option");
			var $highlighted = $options.filter(".highlighted");

			if(param) {
				var $option = 0;
				var currentIndex = 0;

				if(typeof param === "object") {

					// stop any default action and bubbling from occurring in UI
					// all UI updates are handled through updateDropdown
					param.stopPropagation();
					param.preventDefault();

					if($(param.target).hasClass("option")) {
						$option = $(param.target);
					} else {
						$option = $(param.target).closest(".option");
					}
					currentIndex = $options.index($option);

					self.options.index = currentIndex;

					$highlighted.removeClass("highlighted");
					$($options[self.options.index]).addClass("highlighted");

				} else if(typeof param === "number") {
					// check to see if it exists first
					if($($options[param])) {
						self.options.index = param;				
					// if the index does not exist, set index back to 0
					} else {
						self.options.index = 0;
					}
					$highlighted.removeClass("highlighted");
					$($options[self.options.index]).addClass("highlighted");						
				}
			} else {
				$highlighted.removeClass("highlighted");
				$($options[self.options.index]).addClass("highlighted");
			}
		},

		/**
		 * Select currently highlighted option
		 *
		 * Select the currently highlighted dropdown option by triggering a click event.
		 */
		selectOptionByIndex: function() {
			var self = this;
			var $dropdown = self.options.$dropdown;
			var $options = $dropdown.find(".option");
			var $highlighted = $options.filter(".highlighted");
			
			$highlighted.trigger('click');
		},

		/**
		 * Handle events when search input is focused
		 *
		 * When search input is focused, toggle style changes for dropdown
		 *
		 * @param 	{Object} 	ev				The jQuery event object to retrieve the keycode
		 */
		onSearchFocus: function(ev) {
			ev.stopPropagation();
			var self = this;
			var $display = self.options.$display;

			if(!$display) return;
			if($display.hasClass("open")) {
				$display.removeClass("display-focused");
			} else {
				$display.addClass("display-focused");
			}
		},

		/**
		 * Handle events when search input is defocused / blurred
		 *
		 * When search input is defocused, remove focus style
		 *
		 * @param 	{Object} 	ev				The jQuery event object to retrieve the keycode
		 */
		onSearchBlur: function(ev) {
			ev.stopPropagation();
			var self = this;
			var $display = self.options.$display;

			if(!$display) return;
			$display.removeClass("display-focused");
		},

		/**
		 * Handle events when a key is pressed down in search input
		 *
		 * When a key is entered, perform a function pertaining to a collection of functions
		 * to move the option highlight.
		 *
		 * @param 	{Object} 	ev				The jQuery event object to retrieve the keycode
		 */
		onSearchKeydown: function(ev) {
			var self = this;
			ev.stopPropagation();
			var keyCode = ev.keyCode;
			var $dropdown = self.options.$dropdown;
			var key = {
				UP: 38,
				DOWN: 40,
				ENTER: 13,
				ESC: 27,
				TAB: 9,
				SHIFT: 16
			};

			// ignore any input when any value is currently loading
			if(self.options.isLoading) return;

			// if dropdown does not exist, open the dropdown if the user enters anything
			if((!$dropdown || !$dropdown.hasClass("open")) && (keyCode != key.ESC) && (keyCode != key.TAB) && (keyCode != key.SHIFT)) {
				self.open();
				return;
			}

			// handle navigation
			switch(keyCode) {
				case key.UP:
					self.upIndex();
					break;
				case key.DOWN:
					self.downIndex();
					break;
				case key.ENTER:
					self.selectOptionByIndex();
					break;
				default:
					break;
			}
		},

		/**
		 * Handle events when a key is released up in search input
		 *
		 * When a key is entered and released, perform search based on the value in the search input.
		 *
		 * @param 	{Object} 	ev				The jQuery event object to retrieve the keycode
		 */
		onSearchKeyup: function(ev) {
			var self = this;
			ev.stopPropagation();
			var $input = $(ev.target);
			var $searchIcon = $(ev.target).closest(".search-box").find(".cd-search-icon");
			var value = $input.val();
			var keyCode = ev.keyCode;
			var key = {
				UP: 38,
				DOWN: 40,
				ENTER: 13,
				ESC: 27
			};

			// ignore any input when any value is currently loading
			if(self.options.isLoading) return;

			// handle initial states
			if(value) {
				$searchIcon.addClass("active");
				self.options.isSearch = true;
				self.options.state = 'search';
			} else {
				$searchIcon.removeClass("active");
				self.options.isSearch = false;
				self.options.state = 'loaded';
				self._search(true); // reset

				if(keyCode == key.ESC) self.close();
				return; // nothing more to do after this
			}

			// handle navigation
			switch(keyCode) {
				case key.ESC:
					$searchIcon.removeClass("active");
					$input.val("");
					self._search(true); // reset
					self.options.isSearch = false;
					break;
				case key.UP:
					break;
				case key.DOWN:
					break;
				case key.ENTER:
					break;
				default:
					self._search(); // ensured that there is a keyword to be searched
					break;
			}

			// handle bulk select actions
			/*// if bulk select is enabled, look for the trigger token to begin bulk paste mode,
			// otherwise, search as usual
			if(self.options.enable_bulk_select) {
				// look for trigger word, when found, perform different actions based on key press
				if(value.length >= triggerWord.length && value.substring(0, triggerWord.length) == triggerWord) {
					self._search(true); // true = reset search for bulk paste
					self.options.isSearch = false;
					if(keyCode == 27) { // escape
						self._toggleBulkStatus(false); // exit out of bulk status
						$input.val(""); // clear input
					} else if(keyCode == 13) { // enter
						self._bulkSelect(); // immediately select these elements
					} else {
						// generate bulk select status to let users know
						// what it's about to do
						self._toggleBulkStatus(true);
					}
				// otherwise, simply use the input as search
				} else {
					if(keyCode == 27) {
						$input.val(""); // clear input
						self._search(true);
					} else {
						if(!$input.val()) {
							self._search(true);
							self.options.isSearch = false;
						} else {
							self._toggleBulkStatus(false);
							self._search();
						}
					}
				}
			} */

		},

		/**
		 * Start and stop the search delay timer
		 *
		 * Helper function to set and stop the search delay timer between when the user stops
		 * typing and when search procedure begins. Modes increase or decrease search delay times,
		 * as well as clearing the timer.
		 *
		 * @param 	{String} 	mode			Required string to indicate which timer to set / clear
		 * @param 	{Function} 	cb				The callback procedure when timer has gone off
		 */
		toggleSearchTimer: function(mode, cb) {
			var self = this;
			var delay = self.options.search_delay;

			if(mode == "set") {
				self.options.search_timer = setTimeout(function() {
					if(cb) cb();
				}, delay);
			} else if(mode == "quickset") {
				self.options.search_timer = setTimeout(function() {
					if(cb) cb();
				}, 0);
			} else if(mode == "clear") {
				clearTimeout(self.options.search_timer);
			}
		},

		/**
		 * Generate unique ID
		 *
		 * Generates a unique 8 digit (4 by 4) ID to be used to uniquely identify groups and options.
		 * All IDs are completely unique.
		 *
		 * @return {String} 	The generated ID string
		 */
		guid: function() {
			var s4 = function() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			};
			return s4() + '-' + s4();
		},

		/**
		 * Disables the dropdown
		 *
		 * Closes the existing dropdown, if any, and disables it for any input
		 * The dropdown will not be able to be opened when disabled
		 */
		disable: function() {
			var self = this;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;

			if($display) {
				$display.addClass("commondrop-disabled");
			}

			if($dropdown) {
				$display.addClass("commondrop-disabled");
			}

			self.options.isDisabled = true;
			self.lockDropdown(true);
			self.close();
		},

		/**
		 * Enables the dropdown
		 *
		 * Enable the dropdown for uesr input.
		 */
		enable: function() {
			var self = this;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;

			if($display) {
				$display.removeClass("commondrop-disabled");
			}

			if($dropdown) {
				$display.removeClass("commondrop-disabled");
			}

			self.options.isDisabled = false;
			self.lockDropdown(false);
		},

		/**
		 * Get or set the list of selected values
		 *
		 * A public function to get or set the selected options. When no argument is provided, function
		 * outputs an array (for multiple selection dropdown) or a single value (for single selection dropdown)
		 * when the argument is provided, if it is a string, number, or array, the options with the matching value 
		 * will be selected, then the dropdown will be updated. If rawData is a Boolean, it acts as a flag to output
		 * value only, or the full object. Type conversion will not occur for true/false.
		 *
		 * @param 	{(String|Number|Array|Boolean)} 	rawData		Value to select in the form of string or an array | Flag to output full option object
		 *
		 * @return 	{Array} 						Array of selected option values
		 */
		value: function(rawData) {
			var self = this;
			var data = self.options.data;
			var selected = [];
			var $dropdown = self.options.$dropdown;
			var $display = self.options.$display;
			var findMatchingObj = function(val) {
				var match = null;
				if(self.options.enable_groups) {
					$.each(data, function(i, groupObj) {
						$.each(groupObj.data, function(i, obj) {
							if(obj.value == val) {
								match = obj;
								return false;
							}
						});
					});
				} else {
					$.each(data, function(i, obj) {
						if(obj.value == val) {
							match = obj;
							return false;
						}
					});
				}
				
				return match;
			};

			var deselectOptions = function() {
				if(self.options.enable_groups) {
					$.each(self.options.data, function(i, group) {
						$.each(group.data, function(j, dataObj) {
							dataObj.selected = false;
						});
					});
				} else {
					$.each(self.options.data, function(i, option) {
						option.selected = false;
					});
				}
			};

			if(rawData !== undefined && (rawData !== true && rawData !== false)) {
				// resolve types and make all values into an array
				if(typeof rawData == "string") {
					rawData = [rawData];
				} else if(typeof rawData == "number") {
					rawData = [rawData.toString()];
				} else if(Array.isArray(rawData)) {
					if(self.options.selection_mode == "single" && rawData.length > 1) {
						if(self.options.output_logs) throw Error("value: Incompatible value type, you're attempting to select a value type that is not suitable for this type of dropdown.");
						return;
					}

					$.each(rawData, function(i, data) {
						if(typeof data == "number") {
							rawData[i] = rawData[i].toString();
						}
					});
				} else {
					if(self.options.output_logs) throw Error("value: Incompatible value type, you're attempting to select a value type that is not suitable for this type of dropdown.");
					return;
				}

				// first deselect all the options
				deselectOptions();
				// then select all options in the rawData array
				$.each(rawData, function(i, value) {
					// find matching option in data
					var matchingObj = findMatchingObj(value);
					if(matchingObj) {
						matchingObj.selected = true;
					}
				});

				if($dropdown) self.updateDropdown();
				if($display) self.updateDisplay();
				self.setSelect();


			} else {

				var outputFullObject = rawData === true ? true : false;
				if(self.options.enable_groups) {
					$.each(data, function(i, groupObj) {
						$.each(groupObj.data, function(i, obj) {
							if(obj.selected) 
								if(outputFullObject) {
									selected.push(obj);
								} else {
									selected.push(obj.value);
								}
						});
					});
				} else {
					$.each(data, function(i, obj) {
						if(obj.selected) {
							if(outputFullObject) {
								selected.push(obj);
							} else {
								selected.push(obj.value);
							}
						}
					});
				}

				if(selected.length) {
					if(self.options.selection_mode == 'single') {
						return selected[0];
					}
					if(self.options.selection_mode == 'multiple') {
						return selected;
					}
				} else {
					if(self.options.selection_mode == 'single') {
						return null;
					}
					if(self.options.selection_mode == 'multiple') {
						return [];
					}
				}
				return selected;
			}
		},

		/**
		 * Set or get the options data object
		 *
		 * Provided an array of option object with the properties 'text' and 'value', these options are 
		 * bootstrapped with additional option properties (e.g. id, metadata, etc.) and are placed in the 
		 * options data set. Calling this function replaces the entire data set completely with a new one.
		 *
		 * @param 	{Array} 	rawData		An array of objects representing a list of options in the form of value and text
		 *
		 * @return 	{Array} 				Array of options data set used internally
		 */
		data: function(rawData) {
			var self = this;
			var data = [];
			var $dropdown = self.options.$dropdown;
			var $display = self.options.$display;

			var extendOptions = function(rawOptions) {
				var options = [];
				$.each(rawOptions, function(i, option) {
					if(typeof option != 'object') {
						if(self.options.output_logs) throw Error("data: Unknown option, you are not providing a correct object as an option.");
						return true; // skip
					}
					if(option.text === undefined || option.value === undefined) {
						if(self.options.output_logs) throw Error("data: Required properties, you are not providing both text and value as properties in the option object you're trying to create.");
						return true; // skip
					}
					// extend data object with additional options
					options.push($.extend(true, {}, self.options.data_template, option, {
						id: self.guid()
					}));
				});
				return options;
			};

			if(rawData) {
				if(self.options.enable_groups) {
					$.each(rawData, function(i, group) {
						if(typeof group != 'object') {
							if(self.options.output_logs) throw Error("data: Unknown group, you are not providing a correct object as a group.");
							return true; // skip
						}
						if(group.text === undefined || group.data === undefined) {
							if(self.options.output_logs) throw Error("data: Required properties, you are not providing both text and data as properties in the group object you're trying to create.");
							return true; // skip
						}
						// extend data object with additional options
						data.push($.extend(true, {}, self.options.group_template, group, {
							id: self.guid(),
							data: extendOptions(group.data)
						}));
					});
				} else {
					data = extendOptions(rawData);
				}
				self.options.data = data;

				if($dropdown) self.updateDropdown();
				if($display) self.updateDisplay();
				self.setSelect();

			} else {
				return self.options.data;
			}
		},

		/**
		 * Reset dropdown
		 *
		 * Resets the entire dropdown and clears all options. Dropdown will
		 * also be closed.
		 *
		 */
		reset: function() {
			var self = this;

			// first close the dropdown and move the search input
			// back into display
			self.close();

			// reset data sets
			self.options.data = [];

			// reset states
			self.options.state = null;
			self.options.initially_loaded = false;
			self.options.isSearch = false;
			self.options.isLoading = false;
			self.options.isDisabled = false;
			self.options.index = 0;

			self._destroyDropdown();
			self.updateDisplay();
			self.setSelect();
		}
	});

})(jQuery, window, document);