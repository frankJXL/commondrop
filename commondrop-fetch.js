/* ===============================================================================
  _____                           ___                  ____    __      __ 
 / ___/__  __ _  __ _  ___  ___  / _ \_______  ___    / __/__ / /_____/ / 
/ /__/ _ \/  ' \/  ' \/ _ \/ _ \/ // / __/ _ \/ _ \  / _// -_) __/ __/ _ \
\___/\___/_/_/_/_/_/_/\___/_//_/____/_/  \___/ .__/ /_/  \__/\__/\__/_//_/
                                            /_/                           
                                            
Commondrop Fetch - Multipurpose Dropdown
by Frank Liu for Veeva Network

Version 0.1

See usage and documentation at 

!! DO NOT DIRECTLY EDIT THIS FILE !!

================================================================================ */

(function($, window, document, undefined) {
	$.widget("custom.commondropfetch", $.custom.commondrop, {

		options: {
			// feature flags
			load_text: "Loading options...",							// placeholder for loading first cluster of options
			load_search_text: "Searching...",							// placeholder for loading first cluster of search options
																		// fetch procedures are functions that return
			fetch_procedure_value: null,								
			fetch_procedure_cluster: null,				
			fetch_procedure_search: null,

			// data
			data: [],
			search_data: [],											// a separate list of data specifically fetched from search

			// fetch states			
			fetch_states: {
				data_total: null,										// the total number of options available on server
				data_offset: 0,											// the offset position of data from the server
				data_limit: 50,											// the number of options to fetch from the server per call

				dom_offset: 0,											// the number of options that is offsetted at a time in DOM
				dom_limit: 50,											// the number of options that is to be rendered in the DOM at a time
				dom_range: 100  										// the number of options that is to be rendered in the DOM at a time
			},

			search_states: {
				data_total: null,										// the total number of options available on server
				data_offset: 0,											// the offset position of data from the server
				data_limit: 50,											// the number of options to fetch from the server per call

				dom_offset: 0,											// the number of options that is offsetted at a time in DOM
				dom_limit: 50,											// the number of options that is to be rendered in the DOM at a time
				dom_range: 100  										// the number of options that is to be rendered in the DOM at a time
			},
			initially_search_loaded: false,								// set to true when first search cluster is loaded
			allow_empty: true,											// allow the dropdown to be in a deselected state (no value)
			// scroll lock
			scroll_locked: false,										// flag to indicate if the scroll procedure is locked or not, it can be unlocked by reaching the threashold of the scroll_lock_buffer
			scroll_lock_buffer: 0,										// the index number of scroll numbers that has been performed until scroll is unlocked
			scroll_lock_limit: 5										// the counter number of times a scroll has to be performed before scroll procedure is re-enabled
		},

		/**
		 * Enable/Disable features based on existing configuration
		 *
		 * Some features cannot be turned on while others are enabled
		 * this function will disable or enable features that collide with
		 * each other
		 */
		featureCrossCheck: function() {
			var self = this;
			
			self._super();
			// CommonDrop fetch operates in data only, thus options will be overriden
			self.options.keep_options = false;

			// CommonDrop fetch requires search to be enabled
			self.options.enable_search = true;

			// CommonDrop fetch can potentially fetch search results prior to their
			// existance in the actual data set, therefore they should be exclusively
			// pinned to the top
			self.options.pin_selected = true; 

			// CommonDrop fetch does not support grouped options
			self.options.enable_groups = false;
		},

		/**
		 * Main initialization procedure. Creates dropdown and display.
		 *
		 * Adds select options into the data set by creating commondrop option
		 * objects, starts off main event loop after creating options.
		 */
		_create: function() {
			var self = this;
			var newData = [];

			// create a reference to the original element
			self.options.$select = this.element;
			self.options.$select.addClass("commondrop-enabled");

			if (self.options.$select.attr("multiple")) {
				this.options.selection_mode = 'multiple';
			} else {
				this.options.selection_mode = 'single';
			}

			if(self.options.data_override && self.options.data.length) {
				// use data instead
			} else {
				self.options.$select.find("option").each(function() {
					// skip empty options
					if (!$(this).val()) return true;
					newData.push($.extend(true, {}, self.options.data_template, {
						id: self.guid(),
						text: $(this).text(),
						value: $(this).val(),
						selected: $(this).prop("selected"),
						disabled: $(this).prop("disabled")
					}));
				});
				self.options.data = newData;
			}

			// create an empty option if allow_empty option is enabled
			if(self.options.allow_empty && self.options.selection_mode == 'single') {
				newData.push($.extend(true, {}, self.options.data_template, {
					id: self.guid(),
					text: self.options.placeholder_option_text,
					value: null,
					selected: false,
					disabled: false
				}));
			}

			self.options.$select.css({
				visibility: 'none' // needed to calculate location of commondrop, hide after render
			});

			/* ------------------------------------------------------------------- */
			/* MAIN EXECUTION
			/* ------------------------------------------------------------------- */
			// create the display element
			self.featureCrossCheck();
			self.options.$display = self._createDisplay();
			self.element.after(self.options.$display);
			self.options.$select.hide();
			self.repositionSearch();
			self.updateDisplay();
			this._attachHandlers();
			self.setColorStatus();
			/* ------------------------------------------------------------------- */
		},

		/**
		 * Re-render the display component
		 *
		 * Based on display mode and type, render and reposition the display depending
		 * on the selected options and attach handlers to elements such as tags.
		 * In search state, provide updateDisplay with a super set of both the data
		 * from the options data set and the search data set such that selected options
		 * can be shown live even if search is active and no search options have been
		 * merged back into the options data set.
		 */
		updateDisplay: function() {
			var self = this;
			var isSearch = self.options.isSearch;
			var data = self.options.data;
			var searchData = self.options.search_data;
			var allData = [];

			if(isSearch) {
				// pass both data from the data and search data
				allData = allData.concat(data);
				allData = allData.concat(searchData);
			} else {
				allData = data;
			}
			// call original updateDisplay with custom data supplied
			self._super(allData);
		},

		/**
		 * Update any optional features' states
		 *
		 * Update feature states and views (e.g. status, modifiers, action button)
		 * based on the data set when called.
		 */
		updateFeatures: function() {
			var self = this;
			var total = 0;
			var selected = 0;
			var data = null;
			var isSearch = self.options.isSearch;
			var $dropdown = self.options.$dropdown;

			if (!self.options.enable_status) return;

			if(isSearch) {
				data = self.options.search_data;
			} else {
				data = self.options.data;
			}

			// update status
			$.each(data, function(i, obj) {
				if (obj.render && obj.selected) selected++;
				if(obj.render) total++;
			});
			$dropdown.find(".status").text(selected + " / " + total);

			// calculate width for modifiers in accordance to dropdown width
			if(self.options.enable_modifiers) {
				var $modifierBox = $dropdown.find(".modifiers");
				var totalModifiers = self.options.modifiers.length;
				var $modifiers = $modifierBox.find(".modifier");
				var margin = 10;
				var scrollPadding = 30;
				var eachModifierWidth = (($modifierBox.width() - scrollPadding) / totalModifiers) - margin;

				$modifiers.each(function() { $(this).width(eachModifierWidth); });
			}
		},

		/**
		 * Construct dropdown element
		 *
		 * Creates the DOM element for commondrop dropdown, and binds
		 * events to the display element
		 *
		 * @return {element} the display element
		 */
		_renderDropdown: function() {
			var self = this;
			var $dropdown = self._super();
			$dropdown.addClass("commondrop-fetch");
			return $dropdown;
		},

		/**
		 * Create empty placeholder element for dropdown
		 *
		 * Creates the empty placeholder element for dropdown
		 * when no options exist
		 */
		_createEmpty: function() {
			var self = this;
			var $dropdown = self.options.$dropdown;
			var $empty = $("<div></div>").addClass("empty-box");
			
			// dispaly different text based on loading patterns
			$empty.text(self.options.empty_options_text);

			$dropdown.find(".empty-box").remove();
			$dropdown.append($empty);

			self.options.$empty = $empty;
		},

		/**
		 * Create a loader placeholder element for dropdown
		 *
		 * Creates a loader placeholder element when the dropdown
		 * is fetching it's first cluster of data.
		 */
		_createLoading: function() {
			var self = this;
			var isSearch = self.options.isSearch;
			var $dropdown = self.options.$dropdown;
			var $loading = $("<div></div>").addClass("loading-box loader-after");
			if(isSearch) {
				$loading.text(self.options.load_search_text);
			} else {
				$loading.text(self.options.load_text);
			}

			$dropdown.find(".loading-box").remove();
			$dropdown.append($loading);

			self.options.$loading = $loading;	
		},

		/**
		 * Destroy loader placeholder element for dropdown
		 *
		 * Destroys the loader placeholder element and remove it's
		 * reference from options
		 */
		_destroyLoading: function() {
			var self = this;
			var $loading = self.options.$loading;
			if(!$loading) return;
			self.options.$loading.remove();
			self.options.$loading = null;
		},

		/**
		 * Create a loader placeholder option for dropdown
		 *
		 * Creates a loader placeholder element for the end of the
		 * dropdown when the next cluster is being fetched.
		 */
		_createLoadDivider: function() {
			var self = this;

			// fetch divider should be created only once, if it exists, simply exit
			if(self.options.$loadDivider) return;

			var $dropdown = self.options.$dropdown;
			var $optionsBox = $dropdown.find(".options-box");
			var $loader = $("<div></div>").addClass("load-divider loader-before");
			
			$optionsBox.find(".fetch-divider").remove();
			$optionsBox.append($loader);

			self.options.$loadDivider = $loader;
		},

		/**
		 * Destroy loader placeholder option for dropdown
		 *
		 * Destroys the loader placeholder option and remove it's
		 * reference from options
		 */
		_destroyLoadDivider: function() {
			var self = this;
			var $loadDivider = self.options.$loadDivider;
			if(!$loadDivider) return;
			self.options.$loadDivider.remove();
			self.options.$loadDivider = null;
		},

		/**
		 * Create display element
		 *
		 * Creates the DOM element for commondrop display, and binds
		 * events to the display element
		 *
		 * @return {element} the display element
		 */
		_createDisplay: function() {
			var self = this;
			// render base
			var $display = self._super();
			var $loader = $("<div />").addClass("value-loader");
			// add loader
			$display.append($loader);

			return $display;
		},

		/**
		 * Toggle visibility of dropdown
		 *
		 * Open and closes dropdown by toggling class on display
		 */
		_toggleDropdown: function() {
			var self = this;
			var $display = self.options.$display;
			$display.toggleClass("open");
			if ($display.hasClass('open')) {
				self.open();
			} else {
				self.close();
			}
		},

		/**
		 * Toggle visibility of value loader
		 *
		 * Show and hide the loading indicator for display when the dropdown
		 * is attempting to set value
		 *
		 * @param 	{String} mode			The flag to show or hide the loader
		 */
		_toggleValueLoader: function(mode) {
			var self = this;
			var $display = self.options.$display;
			var $caret = $display.find(".arrow");
			var $loader = $display.find(".value-loader");
			
			if (mode == 'show') {
				$caret.hide();
				$loader.show();
			} else if(mode == 'hide') {
				$caret.show();
				$loader.hide();
			}
		},

		/**
		 * Show the dropdown
		 *
		 * Opens the dropdown, repositions the dropdown in accordance
		 * to the display. Create the dropdown if it has not been created before.
		 * And perform first initialized state operations.
		 */
		open: function() {
			var self = this;

			// trigger an event to close all other dropdown
			// forcing only one to be open at a time
			$(window).trigger("commondrop:close_other", {parent: self}); 

			// create the dropdown
			if(!self.options.$dropdown) {
				self.options.$dropdown = self._createDropdown();

				// append it to the DOM
				self.options.$display.after(self.options.$dropdown);

				// load the first set of items if it's the first time loading the dropdown
				if(!self.options.initially_loaded) {
					self._createLoading();
					self.getCluster();
				}
			}

			self.options.$display.addClass("open");
			self.options.$dropdown.addClass("open");
			self.reposition();
			self.repositionSearch();
			self.setColorStatus();

			// always focus on the search as it handles all index events
			// but wait until all other dropdowns have finished their close operation
			setTimeout(function() { 
				self.options.$search.focus(); 
			}, 50);	

			if(self.options.on_open) {
				self.options.on_open(self.options.$dropdown, self.options.$display, self.options.data);
			}
		},

		/**
		 * Hide the dropdown
		 *
		 * Hides the dropdown, reset search state if search is active, 
		 * and perform any close operations.
		 */
		close: function() {
			var self = this;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;

			// close the dropdown
			if (!self.options.$dropdown) return;
			if(!self.options.$dropdown.hasClass("open")) return;

			var $optionsBox = self.options.$dropdown.find(".options-box");
			var $input = self.options.$search;

			$optionsBox.scrollTop(0);
			$display.removeClass("open");
			$dropdown.removeClass("open");

			// if search was active, merge any selected search options
			// into the existing data set
			if(self.options.isSearch) self.mergeSearchStates();

			// reset search by resetting option states
			self._search(true);
			self.options.isSearch = false;
			$input.val("");

			self.updateDisplay();
			self.updateFeatures();
			self.repositionSearch();
			self.options.$search.focus();

			// remove the dropdown
			if(self.options.destroyOnClose) self._destroyDropdown();

			if(self.options.on_close) self.options.on_close(self.options.$dropdown, self.options.$display, self.options.data);
		},

		/**
		 * Merge selected search options into options data set
		 *
		 * When exiting search state, any selected options will be merged back into
		 * the options data set and selected. The original data set is checked with the 
		 * selected search options of it's availability. If a searched option exists, the
		 * option is simply selected. If it does not exist, copy the search option into the
		 * options data set.
		 */
		mergeSearchStates: function() {
			var self = this;
			var data = self.options.data;
			var searchData = self.options.search_data;
			var selected = [];
			var findMatchingOption = function(val) {
				var matchingOption = null;
				$.each(data, function(i, optionObj) {
					if(optionObj.value == val) {
						matchingOption = optionObj;
						return false;
					}
				});
				return matchingOption;
			};
			// gather all selected options
			$.each(searchData, function(i, optionObj) {
				if(optionObj.selected) selected.push(optionObj);
			});

			// if single mode, deselect all options first before selecting
			// another option
			if(self.options.selection_mode == 'single') {
				$.each(data, function(i, optionObj) {
					optionObj.selected = false;
				});
			}

			// cross check searched option values with existing data set
			// if it exists, set selected to true, if it does not, append to the END of
			// the data set

			$.each(selected, function(i, optionObj) {
				var matchingOption = findMatchingOption(optionObj.value);
				if(matchingOption) {
					matchingOption.selected = true;
				} else {
					// provision the new option with a different ID
					// such that updateDropdown will remove the search-option
					// and append with a regular option. Ommiting this step
					// would leave the search-option in the reconcilliation
					// step as the IDs would match
					optionObj = $.extend(optionObj, {id: self.guid()});
					self.options.data.push(optionObj);
				}
			});
		},

		/**
		 * Bootstrap search options with existing option's data
		 *
		 * When a search cluster data is fetched, one or multiple options in that list may
		 * already exist within the fetched options data set. If it does, copy the selected
		 * attribute from the data set into the search cluster. This ensures that if an option
		 * that is already selected in the options data set occurs in the search cluster, that
		 * option is also selected to maintain synced states across the same option.
		 *
		 * @param 	{Array} searchData			A list of unchanged new search cluster data
		 *
		 * @return 	{Array} 					The bootstrapped search cluster data
		 */
		bootstrapSearchCluster: function(searchData) {
			var self = this;
			var data = self.options.data;
			var findMatchingOption = function(val) {
				var matchingOption = null;
				$.each(data, function(i, optionObj) {
					if(optionObj.value == val) {
						matchingOption = optionObj;
						return false;
					}
				});
				return matchingOption;
			};


			// find matching option that exists in the data set
			// if it exists, copy over all of that option's properties
			// onto the search data
			$.each(searchData, function(i, optionObj) {
				var matchingOption = findMatchingOption(optionObj.value);
				if(matchingOption) {
					// copy over all properties from the original option
					searchData[i] = $.extend({}, matchingOption, {
						// render needs to be overriden, since original option render is always false
						// due to it being hidden during search state
						// NOTE that id will be forced to be overwritten in getCluster()
						render: true
					});
				}
			});

			return searchData;
			
		},

		/**
		 * Toggle render property for options data set
		 *
		 * When search is active, existing options are hidden by setting render property
		 * from true to false. When search is inactive, the opposite behaviour is performed.
		 * This function iterates through the options list and toggles the render property and
		 * updates the dropdown.
		 *
		 * @param 	{String} mode				Flag indicating to either hide or show the data set
		 */
		toggleDataset: function(mode) {
			var self = this;
			if(mode == 'show') {
				$.each(self.options.data, function(i, option) {
					option.render = true;
				});
			}
			if(mode == 'hide') {
				$.each(self.options.data, function(i, option) {
					option.render = false;
				});
			}
			// update the dropdown to reflect new render changes
			self.updateDropdown(null, true);
		},

		/**
		 * Sets different state flags for the widget
		 *
		 * Sets different state flags depending on the given explicit state. Also adds and removes
		 * classes from both the display and dropdown to reflect the current state.
		 *
		 * @param 	{String} state				The explicit state to set
		 */
		setState: function(explicitState) {
			var self = this;
			var $display = self.options.$display;
			var $dropdown = self.options.$dropdown;

			if($display) $display.removeClass("loading loaded search search-loading value-loading");
			if($dropdown) $dropdown.removeClass("loading loaded search search-loading value-loading");

			switch (explicitState || self.options.state) {
				case 'loading':
					self.options.isLoading = true;
					if($display) $display.addClass("loading");
					if($dropdown) $dropdown.addClass("loading");
					break;
				case 'loaded':
					self.options.isLoading = false;
					if($display) $display.addClass("loaded");
					if($dropdown) $dropdown.addClass("loaded");
					break;
				case 'search':
					if($display) $display.addClass("search");
					if($dropdown) $dropdown.addClass("search");
					break;
				case 'search-loading':
					self.options.isLoading = true;
					if($display) $display.addClass("search-loading");
					if($dropdown) $dropdown.addClass("search-loading");
					break;
				case 'search-loaded':
					self.options.isLoading = false;
					if($display) $display.addClass("search-loaded");
					if($dropdown) $dropdown.addClass("search-loading");
					break;
				case 'value-loading':
					self.options.isLoading = true;
					if($display) $display.addClass("value-loading");
					if($dropdown) $dropdown.addClass("value-loading");
					break;
			}
		},

		/**
		 * Event handler for dropdown scroll
		 *
		 * Event handler for scrolling anywhere on the dropdown. Option list scroll positions are
		 * calculated and fetching for the next cluster of options are invoked when the scroll position 
		 * reaches the bottom of the list.
		 *
		 * @param 	{Object} 	e				The jQuery event object
		 */
		onDropdownScroll: function(e) {
			var self = this;

			if(e) e.stopPropagation();
			var $optionsBox = 				self.options.$dropdown.find(".options-box");
			var currentBottomPosition = 	$optionsBox.scrollTop() + $optionsBox.height();
			var currentTopPosition = 		$optionsBox.scrollTop();
			var totalOptionsHeight = 		0;
			var buffer = 16;
			var isEndOfData = function() {
				var isSearch = self.options.isSearch;
				var data = self.options.data;
				var searchData = self.options.search_data;
				var fetchStates = self.options.fetch_states;
				var searchStates = self.options.search_states;
				
				if(isSearch) {
					if(searchData.length >= searchStates.data_total) return true;
				} else {
					if(data.length >= fetchStates.data_total) return true;
				}
				return false;
			};
			// do not attempt to pull additional if one is already in process
			if (self.options.state == 'loading') return;

			// if the scroll is locked, and the buffer fills up, clear the lock and 
			// allow procedures to be performed again
			if(self.options.scroll_locked) {
				if(self.options.scroll_lock_buffer >= self.options.scroll_lock_limit) {
					// unlock the scroll and allow procedures to run again
					self.disableScrollLock();
				} else {
					// increase scroll lock buffer
					self.options.scroll_lock_buffer++;
					return; // effectively does not allow scroll procedure to continue
				}
			}

			// get total height of all the options
			totalOptionsHeight = $optionsBox.find(".option:not(.hidden)").length * 30;

			if(self.options.output_logs) console.log("TOP: ", currentTopPosition, " BOTTOM: ", currentBottomPosition, " HEIGHT: ", totalOptionsHeight);

			// load next set of options
			if ((currentBottomPosition + buffer) >= totalOptionsHeight) {
				if(self.options.output_logs) console.log("## BOTTOM ##");
				// determines if the list has reached the end, if it has, simply exit
				if(isEndOfData()) return;
				// show the loader spinner and scroll to bottom of the list
				self._createLoadDivider();

				// scroll 50px to show the loader
				$optionsBox.scrollTop(totalOptionsHeight + 50);

				// get the next set of options and add them to the data
				self.getCluster(function() {
					// scroll to the previously selected position in the list
					$optionsBox.scrollTop(totalOptionsHeight - $optionsBox.height());
				});
				return;
			}
		},

		/**
		 * Event handler for dropdown option click
		 *
		 * Event handler for clicking on an option in the dropdown. Selection
		 * of option starts here.
		 *
		 * @param 	{Object} 	e				The jQuery event object
		 */
		onOptionClick: function(ev) {
			// stop any default action and bubbling from occurring in UI
			// all UI updates are handled through updateDropdown
			ev.stopPropagation();

			var self = this;
			var $option = $(ev.target).is("div") ? $(ev.target) : $(ev.target).closest("div");
			var isSearchOption = $option.hasClass("search-option");
			var matchingOption = null;
			var data = null;
			var optionClass = "search-option";

			// provide different data and set context
			if(isSearchOption) {
				data = self.options.search_data;
			} else {
				data = self.options.data;
			}

			// find the option in set
			$.each(data, function(i, option) {
				if (option.id == $option.attr("option-id")) matchingOption = option;
			});

			// if option is disabled, do nothing
			if (matchingOption.disabled) return;

			// if in single mode, clear selected options
			if (self.options.selection_mode == 'single') {
				$.each(data, function(i, option) { option.selected = false; });
			}

			// toggle the clicked option's selected state
			matchingOption.selected = !matchingOption.selected;

			// update UIs
			if(isSearchOption) {
				self.updateDropdown({
					id: matchingOption.id,
					action: matchingOption.selected ? 'select' : 'deselect'
				}, null, true, data, optionClass);
			} else {
				self.updateDropdown({
					id: matchingOption.id,
					action: matchingOption.selected ? 'select' : 'deselect'
				}, null, true);
			}

			self.updateDisplay();
			self.updateFeatures();

			// close the dropdown after selection
			if (self.options.selection_mode == 'single') {
				self._toggleDropdown();
			}
		},

		triggerChange: function(preventOverride, changeObj) {
			var self = this;
			var initiallyLoadedFactor = null;
			var isSearch = self.options.isSearch;
			var data = isSearch ? self.options.search_data : self.options.data;
			var matchingOption = null;

			if(self.options.isSearch) {
				initiallyLoadedFactor = self.options.initially_search_loaded;
			} else {
				initiallyLoadedFactor = self.options.initially_loaded;
			}

			if(self.options.trigger_change && !preventOverride && initiallyLoadedFactor) {

				// if a changeObj is provided, it indicates that a particular option has been
				// changed exclusively. We can let the user know exactly which option changed
				// by providing the changed value.
				if(changeObj) {
					$.each(data, function(i, dataObj) {
						if (dataObj.id == changeObj.id) {
							matchingOption = dataObj;
							return false;
						}
					});

					self.options.$select.trigger("change", {
						option: matchingOption,
						action: changeObj.action
					});
				} else {
					self.options.$select.trigger("change");
				}
			}
		},

		/**
		 * Set original select's options
		 *
		 * Clear the original select and append option elements back into the original 
		 * select after changes have been made to the data set such that the original select
		 * can be accessed for a list of selected values. If keep_option is false, 
		 * only selected options are created, otherwise, all options and their states are reflected
		 * back into the select element.
		 */
		setSelect: function() {
			var self = this;
			var isSearch = self.options.isSearch;
			var data = self.options.data;
			var searchData = self.options.search_data;
			var $select = self.options.$select;
			var $options = $select.find("option");
			var $optGroups = $select.find("optgroup");

			// first remove all options
			$options.remove();
			$optGroups.remove();

			if(isSearch) {
				// always get the selected data, since it represents the most update to date
				// selected options
				$.each(searchData, function(i, dataObj) {
					// only append selected options
					if(dataObj.selected) $select.append(self.createOptionEl(dataObj));
				});	

				// if the dropdown is a multiple, sample from both the search data and options
				// data set to get the full list of selected options
				if(self.options.selection_mode == 'multiple') {
					$.each(data, function(i, dataObj) {
						// only append selected options
						if(dataObj.selected) $select.append(self.createOptionEl(dataObj));
					});	
				}
			} else {
				// then append back only selected options
				$.each(data, function(i, dataObj) {
					// only append selected options
					if(dataObj.selected) $select.append(self.createOptionEl(dataObj));
				});	
			}
		},

		/**
		 * Performs cluster search
		 *
		 * Searches the non-local list of values by hiding the list of options first, then
		 * getting clusters of search options and appending them onto the dropdown.
		 * 
		 * @param 	{Boolean} reset		Flag indicating if search should end immediately
		 */
		_search: function(reset) {
			var self = this;
			var $input = self.options.$search;
			var value = $input.val();

			// reset all previous lazy loaded search states
			self.toggleSearchTimer("clear");
			self.options.index = 0;
			self.options.search_data = [];
			self.options.search_states = $.extend(true, self.options.search_states, {
				data_total: null,
				data_offset: 0
			});

			if(reset) {
				$input.val("");
				self.options.initially_search_loaded = false;
				self.toggleDataset("show");
			}

			if(reset || !value) return;

			// ** fetch search
			// after a brief delay, kick off fetch search from the backend

			self.toggleSearchTimer("set", function() {
				if(self.options.output_logs) console.log("Fetching more options...");
				if(!self.options.initially_search_loaded) self._createLoading();
				self.getCluster();
				self.toggleDataset("hide");
			});
		},

		/**
		 * Get the next cluster of options
		 *
		 * Get the next cluster of options by fetching it non-locality. The new cluster of
		 * options are cleansed and bootstrapped and then appended onto the options data set.
		 * This function also serves as the starting point for fetching search optiosn as well.
		 * 
		 * @param 	{Function} cb		A callback to perform once the cluster is integrated
		 */
		getCluster: function(cb) {
			var self = this;
			var data = null;
			var fetchStates = null;
			var isSearch = self.options.isSearch;
			var targetState = null;
			var optionClass = null;
			var postFetchOp = function(fetchedData) {

				var uniqueData = [];

				if(self.options.output_logs) console.log("-- New Data --", fetchedData);
				// set new offset and total
				fetchStates.data_offset = fetchedData.offset;
				fetchStates.data_total = fetchedData.total;

				// attach properties to the options
				fetchedData.data = self.remapCluster(fetchedData.data);

				if(isSearch) {
					// make cluster data unique, such that previously selected
					// options that already exist in the data set have their
					// attributes (selected/hidden/disabled etc.) applied to
					// by bootstrapping them onto the search options=
					uniqueData = self.bootstrapSearchCluster(fetchedData.data);
					data = data.concat(uniqueData);
				} else {
					// make cluster data unique, such that previously selected
					// search options that already exist in the data set is not
					// added again (only perform for non-search fetches)
					uniqueData = self.uniquifyCluster(fetchedData.data);
					data = data.concat(uniqueData);
				}
				// write back into data array
				if(isSearch) {
					self.options.search_data = data;
				} else {
					self.options.data = data;
				}

				self._destroyLoading();
				self._destroyLoadDivider();
				self.setState("loaded");
				self.updateDisplay();
				
				if(isSearch) {
					// provide a custom set of options and data set that pertains specifically for search
					// for updateDropdown to use
					data = self.options.search_data;
					optionClass = "search-option";
					self.updateDropdown(null, true, null, data, optionClass); // update the search list
				} else {
					self.updateDropdown(null, true); // update the local list
				}

				self.updateFeatures();
				self.enableScrollLock();
				self.lockDropdown(false); // unlock

				if(isSearch) {
					self.options.initially_search_loaded = true;
				} else {
					self.options.initially_loaded = true;
				}

				self.setIndex();  // set initial highlight index

				if(cb) cb();
			};

			// depending on which mode the dropdown is in, we access different arrays of data
			if(self.options.isSearch) {
				data = self.options.search_data;
				fetchStates = self.options.search_states;
				targetState = "search-loading";
			} else {
				data = self.options.data;
				fetchStates = self.options.fetch_states;
				targetState = "loading";
			}

			// If a cluster is currently being pulled, suspend all operations until it has finished
			if(self.options.isLoading) return;

			// prevent any action from being performed when fetching

			if(!self.options.isSearch) self.lockDropdown(true); // do not lock up the dropdown during search
			self.setState(targetState);

			// If the data set has reached the total length of the options, the list has completed, do not pull again
			// note that on initial fetch, the data total is null
			if((fetchStates.data_total != null) && (data.length >= fetchStates.data_total)) return;

			// fetch a set of options based on the current offset and the limit of options given
			// then append the data onto the dropdown

			if(self.options.isSearch) {
				self.fetchSearch(postFetchOp);
			} else {
				self.fetchCluster(postFetchOp);
			}

		},

		/**
		 * Lock scroll events
		 *
		 * Prevent scroll event from firing by setting a flag.
		 */
		enableScrollLock: function() {
			var self = this;
			self.options.scroll_locked = true;
			self.options.scroll_lock_buffer = 0;
			if(self.options.output_logs) console.log("Scroll locked!");
		},

		/**
		 * Unlocks scroll events
		 *
		 * Allows scroll events to occur by setting a flag.
		 */
		disableScrollLock: function() {
			var self = this;
			self.options.scroll_locked = false;
			self.options.scroll_lock_buffer = 0;
			if(self.options.output_logs) console.log("Scroll unlocked!");
		},

		/**
		 * Bootstrap additional properties onto cluster options
		 *
		 * Add additional properties (e.g. IDs) onto newly fetched cluster options.
		 *
		 * @return 	{Array} 		Boostrapped options
		 */
		remapCluster: function(fetchedData, customProperties) {
			var self = this;
			var options = [];

			$.each(fetchedData, function(i, option) {
				if(typeof option != 'object') {
					if(self.options.output_logs) console.log("Unknown option", option);
					return true; // skip
				}
				if(option.text === undefined || option.value === undefined) {
					if(self.options.output_logs) console.log("Unknown option", option);
					return true; // skip
				}
				// extend data object with additional options
				options.push($.extend(true, {}, self.options.data_template, option, {
					id: self.guid()
				}, customProperties));
			});
			return options;
		},

		/**
		 * Remove duplicate options between fetched data and options data set
		 *
		 * Take the newly fetched cluster of options and compare their value with the existing
		 * data set of options. Any new options that already exists in the dropdown are removed
		 * to ensure that new clusters can only contain new options that has not been fetched already.
		 *
		 * @return 	{Array} 		A unique fetched data cluster with no cross duplicates
		 */
		uniquifyCluster: function(fetchedData) {
			var self = this;
			var data = self.options.data;
			var uniqueData = [];
			var findMatchingOption = function(val) {
				var matchingOption = null;
				$.each(data, function(i, optionObj) {
					if(optionObj.value == val) {
						matchingOption = optionObj;
						return false;
					}
				});
				return matchingOption;
			};

			$.each(fetchedData, function(i, option) {
				var matchingOption = findMatchingOption(option.value);
				// only push options that do not exist within the data set
				if(!matchingOption) uniqueData.push(option);
			});

			return uniqueData;
		},

		/**
		 * Fetches the initial option's value
		 *
		 * Calls the procedure to fetch the initial option's value and text. This function is generally
		 * used for initializing selects that has value but no associated text with it's option.
		 *
		 * @param 	{Function} 		A callback when fetching is complete
		 */
		fetchValue: function(cb, values) {
			var self = this;
			var dfd = null;
			dfd = self.options.fetch_procedure_value({
				values: values,
				$select: self.options.$select
			});

			if(!dfd) {
				if(self.options.output_logs) throw Error("fetchValue: No promise object provided.");
			}

			dfd.done(function(fetchedData) {

				if(!fetchedData || !Array.isArray(fetchedData)) {
					if(self.options.output_logs) throw Error("fetchValue: Returned data must be an array of text/value pair.");
				}

				if(cb) cb(fetchedData);
			}, function() {
				// fail gracefully
				if(cb) cb([]);
			});
		},

		/**
		 * Fetches a cluster of options
		 *
		 * Calls the procedure to fetch a cluster of options. This function is generally
		 * used when users reach the bottom of the dropdown and requires new options to be loaded
		 * and appended.
		 *
		 * @param 	{Function} 		A callback when fetching is complete
		 */
		fetchCluster: function(cb) {
			var self = this;
			var dfd = null;
			var fetchStates = self.options.fetch_states;
			dfd = self.options.fetch_procedure_cluster({
				$select: self.options.$select,
				offset: fetchStates.data_offset,
				limit: fetchStates.data_limit
			});

			if(!dfd) {
				if(self.options.output_logs) throw Error("fetchValue: No promise object provided.");
			}

			dfd.then(function(fetchedData) {

				if(!fetchedData || !Array.isArray(fetchedData.data)) {
					if(self.options.output_logs) throw Error("fetchValue: Returned data must be an array of text/value pair.");
				}

				if(cb) cb(fetchedData);
			}, function() {
				// fail gracefully, set the same data offset and total, and return no options
				if(cb) cb({
					data: [],
					offset: fetchStates.data_offset,
					total: fetchStates.total
				});
			});
		},

		/**
		 * Fetches a cluster of search options
		 *
		 * Calls the procedure to fetch a cluster of options. This function is generally
		 * used when users perform a search and requires a list of search options to return
		 * as a result.
		 *
		 * @param 	{Function} 		A callback when fetching is complete
		 */
		fetchSearch: function(cb) {
			var self = this;
			var dfd = null;
			var $input = self.options.$search;
			var fetchStates = self.options.search_states;
			dfd = self.options.fetch_procedure_search({
				$select: self.options.$select,
				search: $input.val(),
				offset: fetchStates.data_offset,
				limit: fetchStates.data_limit
			});

			if(!dfd) {
				if(self.options.output_logs) throw Error("fetchValue: No promise object provided.");
			}

			dfd.then(function(fetchedData) {
				if(!fetchedData || !Array.isArray(fetchedData.data)) {
					if(self.options.output_logs) throw Error("fetchValue: Returned data must be an array of text/value pair.");
				}
				if(cb) cb(fetchedData);
			}, function() {
				// fail gracefully, set the same data offset and total, and return no options
				if(cb) cb({
					data: [],
					offset: fetchStates.data_offset,
					total: fetchStates.total
				});
			});
		},

		/**
		 * Get or set the list of selected values
		 *
		 * A public function to get or set the selected options. When no argument is provided, function
		 * outputs an array (for multiple selection dropdown) or a single value (for single selection dropdown)
		 * when the argument is provided, if it is a string, number, or array, the options with the matching value 
		 * will be selected, then the dropdown will be updated. If rawData is a Boolean, it acts as a flag to output
		 * value only, or the full object. Type conversion will not occur for true/false. The rawData is checked with
		 * the existing data set to see if it exists or not. If some values do not exist in the options data set, they
		 * will be fetched using the fetch_procedure_value call. 
		 *
		 * @param 	{(String|Number|Array|Boolean)} 	rawData		Value to select in the form of string or an array | Flag to output full option object
		 *
		 * @return 	{Array} 						Array of selected option values
		 */
		value: function(rawData) {
			var self = this;
			var isSearch = self.options.isSearch;
			var data = self.options.data;
			var searchData = self.options.search_data;
			var nonLocalValues = [];
			var output = [];
			var $dropdown = self.options.$dropdown;
			var $display = self.options.$display;
			var opDfd = new $.Deferred();
			var findMatchingObj = function(val) {
				var match = null;
				$.each(data, function(i, obj) {
					if(obj.value == val) {
						match = obj;
						return false;
					}
				});
				
				return match;
			};

			var deselectOptions = function() {
				$.each(self.options.data, function(i, option) {
					option.selected = false;
				});
			};

			if(rawData !== undefined && (rawData !== true && rawData !== false)) {
				// resolve types and make all values into an array
				if(typeof rawData == "string") {
					rawData = [rawData];
				} else if(typeof rawData == "number") {
					rawData = [rawData.toString()];
				} else if(Array.isArray(rawData)) {
					if(self.options.selection_mode == "single" && rawData.length > 1) {
						if(self.options.output_logs) throw Error("value: Incompatible value type, you're attempting to select a value type that is not suitable for this type of dropdown.");
						return;
					}

					$.each(rawData, function(i, data) {
						if(typeof data == "number") {
							rawData[i] = rawData[i].toString();
						}
					});
				} else if(rawData === null) {
					rawData = [null]; // select the empty option
				} else {
					if(self.options.output_logs) throw Error("value: Incompatible value type, you're attempting to select a value type that is not suitable for this type of dropdown.");
					return;
				}

				// ** OPERATION START ========================================= ** //

				// first lock dropdown and display and deselect all the options
				self.setState("value-loading");
				self._toggleValueLoader("show");
				self.lockDropdown(true); // lock
				deselectOptions();
				// then select all options in the rawData array
				$.each(rawData, function(i, value) {
					// find matching option in data
					var matchingObj = findMatchingObj(value);
					// if a value exists in the dropdown already, select it
					if(matchingObj) {
						matchingObj.selected = true;
					// otherwise, push to nonLocalValues and fetch later
					} else {
						nonLocalValues.push(value);
					}
				});

				// fetch values that does not exist in the dropdown using the 
				// fetch_procedure_value call and add them to the dropdown
				if(nonLocalValues.length) {
					self.fetchValue(function(fetchedData) {
						fetchedData = self.remapCluster(fetchedData, {
							selected: true // set all values to selected
						});
						self.options.data = self.options.data.concat(fetchedData);
						opDfd.resolve();
					}, nonLocalValues);
				} else {
					opDfd.resolve();
				}

				// wait until both local and fetch selection has finished (opPromise is resolved)
				$.when(opDfd.promise()).done(function() {
					if($dropdown) {
						self.updateDropdown();
					} else {
						self.setSelect();
					}
					if($display) self.updateDisplay();
					self._toggleValueLoader("hide");
					self.lockDropdown(false); // unlock
					self.setState("loaded");
				});

			} else {

				var outputFullObject = rawData === true ? true : false;
				
				// include search results too if value is called during search, since search options
				// and regular options exist in two separate data sets.
				if(isSearch) {
					$.each(searchData, function(i, obj) {
						if(obj.selected) {
							if(outputFullObject) {
								output.push(obj);
							} else {
								output.push(obj.value);
							}
						}
					});	

					if(self.options.selection_mode == 'multiple') {
						$.each(data, function(i, obj) {
							if(obj.selected) {
								if(outputFullObject) {
									output.push(obj);
								} else {
									output.push(obj.value);
								}
							}
						});	
					}
				} else {
					$.each(data, function(i, obj) {
						if(obj.selected) {
							if(outputFullObject) {
								output.push(obj);
							} else {
								output.push(obj.value);
							}
						}
					});
				}

				if(output.length) {
					if(self.options.selection_mode == 'single') {
						return output[0];
					}
					if(self.options.selection_mode == 'multiple') {
						return output;
					}
				} else {
					if(self.options.selection_mode == 'single') {
						return null;
					}
					if(self.options.selection_mode == 'multiple') {
						return [];
					}
				}
				return output;
			}
		},

		/**
		 * Get the options data object
		 *
		 * Get the entire data set of options. 
		 *
		 * @return 	{Array} 				Array of options data set used internally
		 */
		data: function(params) {
			var self = this;
			if(params && self.options.output_logs) throw Error("data: Setting of data is unsupported in Commondrop Fetch.");
			return self.options.data;
		},

		/**
		 * Reset dropdown
		 *
		 * Resets the entire dropdown and all fetch states, all options will be removed
		 * and the dropdown will be closed.
		 *
		 */
		reset: function() {
			var self = this;

			self._super();

			// recreate an empty option if allow_empty option is enabled
			if(self.options.allow_empty && self.options.selection_mode == 'single') {
				self.options.data.push($.extend(true, {}, self.options.data_template, {
					id: self.guid(),
					text: self.options.placeholder_option_text,
					value: null,
					selected: false,
					disabled: false
				}));
			};

			// reset data sets
			self.options.search_data = [];

			// reset fetch states
			self.options.fetch_states.data_total = null;
			self.options.fetch_states.data_offset = 0;

			// reset initial states
			self.options.initially_search_loaded = false;

			self.updateDropdown();
			self.updateDisplay();
		}
	});
})(jQuery, window, document);